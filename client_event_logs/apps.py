from django.apps import AppConfig


class ClientEventLogsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'client_event_logs'
