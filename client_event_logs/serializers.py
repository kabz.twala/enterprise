from django.db import transaction
from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import ValidationError

from constance import config
from rest_framework import serializers
from rest_framework.reverse import reverse as api_reverse
from versatileimagefield.serializers import VersatileImageFieldSerializer

from client_event_logs.models import ClientEvent


class ClientEventSerializer(serializers.ModelSerializer):

    class Meta:
        model = ClientEvent
        fields = ('id', 'user', 'site', 'action', 'details', 'data')
        read_only_fields = ('id', 'site')
    
                
    def create(self, validated_data):
        
        if self.context['request'].user.is_authenticated:
            with transaction.atomic():

                instance = ClientEvent.objects.create(**validated_data)
                return instance
                
        raise serializers.ValidationError('User needs to be signed in')

    def update(self, instance, validated_data):

        raise serializers.ValidationError('Not permitted to update')
