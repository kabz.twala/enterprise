from django.db import models
from django.conf import settings
from django.db.models import JSONField
from django.contrib.sites.models import Site
from django.contrib.sites.shortcuts import get_current_site

from enterprise.middleware.get_request import get_request
from client_utils.models import CreatedModifiedMixin, PermissionsMixin

class ClientEvent(CreatedModifiedMixin, PermissionsMixin):

    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.SET_NULL)

    site = models.OneToOneField(Site, null=True, blank=True, on_delete=models.SET_NULL)

    action = models.CharField(max_length=255, blank=False, null=False)

    details = models.TextField(null=True, blank=True)

    # Raw data
    data = JSONField(blank=True, null=True)

    def __str__(self):
        return "{}. {}".format(self.id, self.action)
    
    @classmethod
    def get_queryset(cls, user):
        request = get_request()

        if not user.is_authenticated:
            return ClientEvent.objects.none()

        if user.is_superuser or user.is_staff:
            return ClientEvent.objects.all()

        return ClientEvent.objects.filter(user__id=user.id, site=get_current_site(request))