from django.contrib import admin

from client_event_logs.models import ClientEvent

class ClientEventAdmin(admin.ModelAdmin):
    
    def has_add_permission(self, request, obj=None):
        return False
    
    # This will help you to disable delete functionaliyt
    # def has_delete_permission(self, request, obj=None):
    #     return False

    # This will help you to disable change functionality
    def has_change_permission(self, request, obj=None):
        return False
        
    class Meta:
        model = ClientEvent
        
    search_fields = [
        'site',
        'action',
        'details'
    ]
    list_display = (
        '__str__',
        'site',
        'data',
    )

admin.site.register(ClientEvent, ClientEventAdmin)