from rest_framework import viewsets
from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from client_event_logs.models import ClientEvent
from client_event_logs.serializers import ClientEventSerializer
from client_utils.permissions import IsStaffOrSuperuser
from client_utils.pagination import ClientCustomPagination


class ClientEventLogViewSet(viewsets.ModelViewSet):

    model = ClientEvent
    serializer_class = ClientEventSerializer
    permission_classes = (IsAuthenticated, IsStaffOrSuperuser)
    pagination_class = ClientCustomPagination
    search_fields = (
        'user__username',
        'user__email',
        'action',
    )
    ordering_fields = (
        'id',
        'action',
        'created',
        'modified'
    )

    def get_queryset(self):
        
        if not self.request.user.is_authenticated:
            raise PermissionDenied()

        if not self.request.user.is_superuser or not self.request.user.is_staff:
            return True

        queryset = ClientEvent.get_queryset(self.request.user)

        if queryset is not None:
            return queryset.order_by('-modified')

        raise PermissionDenied()
