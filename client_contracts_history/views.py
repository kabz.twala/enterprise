from rest_framework import viewsets
from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from client_contracts_history.models import ClientContractHistory
from client_contracts_history.serializers import ClientContractHistorySerializer
from client_utils.permissions import ModelBasedPermissions
from client_utils.pagination import ClientCustomPagination


class ClientContractHistoryPermissions(ModelBasedPermissions):
    model = ClientContractHistory
    message = "You don't have permission to access the contract history"


class ClientContractHistoryViewSet(viewsets.ModelViewSet):

    model = ClientContractHistory
    serializer_class = ClientContractHistorySerializer
    permission_classes = (IsAuthenticated, ClientContractHistoryPermissions)
    pagination_class = ClientCustomPagination
    search_fields = (
        'contract',
        'details',
    )
    ordering_fields = (
        'id',
        'previous_contract_start',
        'previous_contract_end',
        'current_contract_start',
        'current_contract_end',
        'created',
        'modified'
    )

    def get_queryset(self):
        
        if not self.request.user.is_authenticated:
            raise PermissionDenied()

        queryset = ClientContractHistory.get_queryset(self.request.user)

        if queryset is not None:
            return queryset.order_by('-modified')

        raise PermissionDenied()
