from django.contrib.sites.shortcuts import get_current_site

from constance import config
from rest_framework import serializers
from rest_framework.reverse import reverse as api_reverse

from client_contracts_history.models import ClientContractHistory
from client_contracts.serializers import ClientContractSerializer


class ClientContractHistorySerializer(serializers.ModelSerializer):
    
    contract = ClientContractSerializer(read_only=True)

    client_contract_history_uri = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = ClientContractHistory
        fields = ('id', 'contract', 'creator', 'previous_package_id', 'current_package_id', 'previous_contract_start', 'previous_contract_end', 'current_contract_start', 'current_contract_end', 'details', 'client_contract_history_uri')
        read_only_fields = ('id', 'contract', 'creator', 'previous_package_id', 'current_package_id', 'previous_contract_start', 'previous_contract_end', 'current_contract_start', 'current_contract_end', 'details', 'client_contract_history_uri')
                
    def get_client_contract_history_uri(self, obj):
        return self.context['request'].build_absolute_uri("/api/enterprise/client-contract-history/{id}/".format(id=obj.id))
        # request = self.context.get('request')
        # return api_reverse('enterprise-clients-contract-history-detail', kwargs={'id': obj.id}, request=request) e.g. -list, -detail, python manage.py show_urls
    
    def create(self, validated_data):
        
        raise serializers.ValidationError('Not permitted to create')

    def update(self, instance, validated_data):
        
        raise serializers.ValidationError('Not permitted to update')
