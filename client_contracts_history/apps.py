from django.apps import AppConfig


class ClientContractsHistoryConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'client_contracts_history'
