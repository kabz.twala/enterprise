from django.contrib import admin

from client_contracts_history.models import ClientContractHistory

class ClientContractHistoryAdmin(admin.ModelAdmin):
    
    def has_add_permission(self, request, obj=None):
        return False
    
    # This will help you to disable delete functionaliyt
    # def has_delete_permission(self, request, obj=None):
    #     return False

    # This will help you to disable change functionality
    def has_change_permission(self, request, obj=None):
        return False
        
    class Meta:
        model = ClientContractHistory
        
    search_fields = [
        'contract',
        'details',
    ]
    list_display = (
        '__str__',
        'contract',
        'previous_package_id',
        'current_package_id',
        'previous_contract_start',
        'current_contract_start',
        'details',
    )

admin.site.register(ClientContractHistory, ClientContractHistoryAdmin)