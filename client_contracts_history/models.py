from django.db import models
from django.conf import settings

from client_contracts.models import ClientContract
from enterprise.middleware.get_request import get_request
from client_utils.models import CreatedModifiedMixin, PermissionsMixin
from client_utils.permissions import get_privileges, get_privileges_exists_check

class ClientContractHistory(CreatedModifiedMixin, PermissionsMixin):
    '''
    Gets updated when the client's contract is altered i.e. upgraded, terminated, not_paid or cancelled
    '''

    contract = models.ForeignKey(ClientContract, blank=True, null=True, on_delete=models.SET_NULL)

    creator = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL)

    previous_package_id = models.PositiveIntegerField(blank=True, null=True)
    current_package_id = models.PositiveIntegerField(blank=True, null=True)

    previous_contract_start = models.DateTimeField(blank=True, null=True)
    previous_contract_end = models.DateTimeField(blank=True, null=True)

    current_contract_start = models.DateTimeField(blank=True, null=True)
    current_contract_end = models.DateTimeField(blank=True, null=True)

    details = models.TextField(blank=True, null=True)

    def __str__(self):
        if self.contract:
            return "{}. {}".format(self.id, self.contract.package.name)
        return str(self.id)
    
    @classmethod
    def get_queryset(cls, user):

        if not user.is_authenticated:
            return ClientContractHistory.objects.none()

        if user.is_superuser or user.is_staff:
            return ClientContractHistory.objects.all()

        contracts = list( user.client_priviledged_user.filter(accepted=True, active=True, can_read_contract_history=True).values_list('contract_id', flat=True) )
        return ClientContractHistory.objects.filter(contract_id__in=contracts)

    @classmethod
    def has_permission(cls, request):
        """ 
        Permissions for Privileged users.
        """

        if request.user.is_superuser or request.user.is_staff:
            return True

        return get_privileges_exists_check(request.user) 

    def has_object_permission(self, request, obj):
        """ 
        Object Permissions for Privileged users.
        """

        if request.user.is_superuser or request.user.is_staff:
            return True

        privileges = get_privileges(request.user) 

        if privileges:

            if request.method == 'GET':
                if obj:
                    return privileges.filter(contract_id=obj.contract.id, can_read_contract_history=True).exists()
                return privileges.filter(can_read_contract_history=True).exists()

        return False