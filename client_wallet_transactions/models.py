from collections import namedtuple
from django.db import models
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site

from enterprise.middleware.get_request import get_request
from client_utils.models import CreatedModifiedMixin, PermissionsMixin
from client_utils.views import get_field_choices, random_generator
from client_utils.permissions import get_privileges, get_privileges_exists_check
from clients.models import Client
from client_invoices.models import ClientInvoice

class ClientWalletTransaction(CreatedModifiedMixin, PermissionsMixin):

    client = models.ForeignKey(Client, null=True, blank=True, on_delete=models.SET_NULL)

    invoice = models.ForeignKey(ClientInvoice, null=True, blank=True, on_delete=models.SET_NULL)

    wallet_reference = models.CharField(max_length=255, null=True, blank=True)

    client_previous_wallet_amount = models.DecimalField(max_digits=8, decimal_places=2, null=True, blank=True)

    transaction_amount = models.DecimalField(max_digits=8, decimal_places=2, null=False, blank=False)
  
    # payment transaction - used to pay invoices
    # top_up transaction - used to pay 
    transaction_choices = (
        'top_up',
        'payment',
        'refund',
    )
    TRANSACTION_TYPE = namedtuple('TRANSACTION_TYPE', transaction_choices)(*range(0, len(transaction_choices)))
    transaction = models.PositiveIntegerField(default=0, choices=get_field_choices(TRANSACTION_TYPE))
    transaction_details = models.TextField(null=True, blank=True)

    def __str__(self):
        return "{}. {}".format(self.id, self.client.name)

    def save(self, *args, **kwargs):

        if not self.wallet_reference:
            while True:
                wallet_reference = random_generator(length=9, letters=True, digits=True, punctuation=False)
                if not type(self).objects.filter(wallet_reference=wallet_reference).exists():
                    break
            self.wallet_reference = wallet_reference
            
        super(ClientWalletTransaction, self).save(*args, **kwargs)

        if self.transaction == 1:
            # payment
            self.client.wallet = float(self.client.wallet) - float(self.transaction_amount)
        else:
            # refund or topup
            self.client.wallet = float(self.client.wallet) + float(self.transaction_amount)

        self.client.save()

    
    @classmethod
    def get_queryset(cls, user):

        if not user.is_authenticated:
            return ClientWalletTransaction.objects.none()

        if user.is_superuser or user.is_staff:
            return ClientWalletTransaction.objects.all()

        clients = list( user.client_priviledged_user.filter(accepted=True, active=True, can_read_wallet_transactions=True).values_list('contract__client_id', flat=True) )
        return ClientWalletTransaction.objects.filter(client_id__in=clients)

    @classmethod
    def has_permission(cls, request):
        """ 
        Permissions for Privileged users.
        """

        if request.user.is_superuser or request.user.is_staff:
            return True

        return get_privileges_exists_check(request.user) 

    def has_object_permission(self, request, obj):
        """ 
        Object Permissions for Privileged users.
        """

        if request.user.is_superuser or request.user.is_staff:
            return True

        privileges = get_privileges(request.user) 

        if privileges:

            if request.method == 'GET':
                if obj:
                    return privileges.filter(contract__client_id=obj.client.id, can_read_wallet_transactions=True).exists()
                return privileges.filter(can_read_wallet_transactions=True).exists()
        
        return False


class ClientIncorrectReferenceWalletTransaction(CreatedModifiedMixin, PermissionsMixin):

    payment_reference = models.CharField(max_length=255, null=True, blank=True)

    amount = models.DecimalField(max_digits=8, decimal_places=2, null=True, blank=True)

    transaction_choices = (
        'unresolved',
        'resolved',
    )
    TRANSACTION_TYPE = namedtuple('TRANSACTION_TYPE', transaction_choices)(*range(0, len(transaction_choices)))
    transaction = models.PositiveIntegerField(default=0, choices=get_field_choices(TRANSACTION_TYPE))
    transaction_details = models.TextField(null=True, blank=True)
    
    handled_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL)
    log_details = models.TextField(null=True, blank=True)

    @classmethod
    def get_queryset(cls, user):

        if user.is_superuser or user.is_staff:
            return ClientIncorrectReferenceWalletTransaction.objects.all()

        return ClientIncorrectReferenceWalletTransaction.objects.none()