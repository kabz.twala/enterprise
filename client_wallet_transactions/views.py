from django.db import transaction
from django.utils import timezone

from rest_framework import decorators, status, viewsets
from rest_framework.response import Response
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated

from client_wallet_transactions.models import ClientWalletTransaction, ClientIncorrectReferenceWalletTransaction
from client_wallet_transactions.serializers import ClientWalletTransactionSerializer, ClientIncorrectReferenceWalletTransactionSerializer
from client_utils.permissions import IsStaffOrSuperuser, ModelBasedPermissions
from client_utils.pagination import ClientCustomPagination


class ClientWalletTransactionPermissions(ModelBasedPermissions):
    model = ClientWalletTransaction
    message = "You don't have permission to access wallet transactions"


class ClientWalletTransactionViewSet(viewsets.ModelViewSet):

    model = ClientWalletTransaction
    serializer_class = ClientWalletTransactionSerializer
    permission_classes = (IsAuthenticated, ClientWalletTransactionPermissions)
    pagination_class = ClientCustomPagination
    search_fields = (
        'client',
        'wallet_reference',
        'transaction_details',
    )
    ordering_fields = (
        'id',
        'client',
        'transaction',
        'created',
        'modified'
    )

    def get_queryset(self):
        
        if not self.request.user.is_authenticated:
            raise PermissionDenied()

        queryset = ClientWalletTransaction.get_queryset(self.request.user)

        if queryset is not None:
            return queryset.order_by('-modified')

        raise PermissionDenied()
    
    @decorators.action(detail=False, methods=['get'], permission_classes = (IsAuthenticated, ))
    def get_client_payment_reference(self, request):

        from clients.models import Client

        try:
            client = Client.objects.filter(creator=request.user).values('id', 'name', 'active', 'payment_reference', 'created', 'modified')
            return Response({'result': client}, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)

    @decorators.action(detail=False, methods=['post'], permission_classes = (IsAuthenticated, IsStaffOrSuperuser), url_path="top-up")
    def top_up(self, request):
        
        from clients.models import Client

        try:
            amount = request.data.get('amount')
            payment_reference = request.data.get('payment_reference')
            
            try:
                with transaction.atomic():

                    client = Client.objects.get(payment_reference=payment_reference)

                    wallet_transaction = ClientWalletTransaction.objects.create(
                        client=client,
                        client_previous_wallet_amount=client.wallet,
                        transaction_amount=amount,
                        transaction=0, 
                        transaction_details="{} - Top up successful".format( timezone.localtime(timezone.now()) )
                    )

                    client.send_email(
                        origin='ClientWalletTransactionViewSet.top_up', 
                        details='Top up successful: {}. {} - #{}'.format(client.id, client.name, client.payment_reference),
                        recipients=[client.creator.id],
                        status=0, 
                        subject='Top up successful - #{}'.format(client.payment_reference), 
                        message_body='The top up for {}. {} - #{}, was successful.'.format(client.id, client.name, client.payment_reference), 
                        view_in_browser_url=request.build_absolute_uri('/api/enterprise/client-wallet-transactions/{}/'.format(wallet_transaction.id))
                    )
                    
                    return Response({'detail': 'Top up successful'}, status=status.HTTP_200_OK)
            
            except Exception as e:

                ClientIncorrectReferenceWalletTransaction.objects.create(
                    payment_reference = payment_reference,
                    amount = amount,
                    transaction_details = 'Incorrect payment reference used',
                    log_details = '{} - {}'.format( timezone.localtime(timezone.now()), str(e) )
                )

                return Response({'detail': 'Incorrect payment reference used, please send proof of payment via email'}, status=status.HTTP_200_OK)
        
        except Exception as e:

            return Response({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)


class ClientIncorrectWalletTransactionViewSet(viewsets.ModelViewSet):

    model = ClientIncorrectReferenceWalletTransaction
    serializer_class = ClientIncorrectReferenceWalletTransactionSerializer
    permission_classes = (IsAuthenticated, IsStaffOrSuperuser)
    pagination_class = ClientCustomPagination
    search_fields = (
        'payment_reference', 
        'amount', 
        'transaction_details', 
        'handled_by', 
        'log_details'
    )
    ordering_fields = (
        'id', 
        'payment_reference', 
        'transaction', 
        'handled_by', 
        'created',
        'modified'
    )

    def get_queryset(self):
        
        if not self.request.user.is_authenticated:
            raise PermissionDenied()

        if not self.request.user.is_superuser or not self.request.user.is_staff:
            raise PermissionDenied('User is not permitted')
            
        queryset = ClientIncorrectReferenceWalletTransaction.get_queryset(self.request.user)

        if queryset is not None:
            return queryset.order_by('-modified')

        raise PermissionDenied()

    @decorators.action(detail=False, methods=['put'], permission_classes = (IsAuthenticated, IsStaffOrSuperuser))
    def correct_wallet_transaction(self, request):

        from clients.models import Client

        try:
            id = request.data.get('id')
            details = request.data.get('details')
            payment_reference = request.data.get('payment_reference')

            incorrect = ClientIncorrectReferenceWalletTransaction.objects.get(id=id)

            try:
                with transaction.atomic():

                    if incorrect.transaction == 1:
                        return Response({'detail': 'Reference has already been resolved'}, status=status.HTTP_400_BAD_REQUEST)

                    incorrect.transaction = 1
                    incorrect.transaction_details = incorrect.transaction_details + "\n\n{} - {}".format( timezone.localtime(timezone.now()), details)
                    incorrect.handled_by = request.user

                    amount = incorrect.amount

                    client = Client.objects.get(payment_reference=payment_reference)

                    wallet_transaction = ClientWalletTransaction.objects.create(
                        client=client,
                        client_previous_wallet_amount=client.wallet,
                        transaction_amount=amount,
                        transaction=0, 
                        transaction_details="{} - Incorrect reference resolved - Top up successful".format( timezone.localtime(timezone.now()) )
                    )

                    client.send_email(
                        origin='ClientIncorrectWalletTransactionViewSet.correct_wallet_transaction', 
                        details='Incorrect reference resolved - Top up successful: {}. {} - #{}'.format(client.id, client.name, client.payment_reference),
                        recipients=[client.creator.id],
                        status=0, 
                        subject='Top up successful - #{}'.format(client.payment_reference), 
                        message_body='The top up for {}. {} - #{}, was successful.'.format(client.id, client.name, client.payment_reference), 
                        view_in_browser_url=request.build_absolute_uri('/api/enterprise/client-wallet-transactions/{}/'.format(wallet_transaction.id))
                    )
                    
                    incorrect.save()

                    return Response({'detail': 'Resolved'}, status=status.HTTP_200_OK)
                    
            except Exception as e:
                incorrect.log_details = "{}\n\n{} - {}".format(incorrect.log_details, timezone.localtime(timezone.now()), e)
                incorrect.save()
                return Response({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            return Response({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)