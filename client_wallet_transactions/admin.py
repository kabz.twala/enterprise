from django.contrib import admin

from client_wallet_transactions.models import ClientWalletTransaction, ClientIncorrectReferenceWalletTransaction

from constance import config


class ClientWalletTransactionAdmin(admin.ModelAdmin):
    
    def has_add_permission(self, request, obj=None):
        return config.CLIENT_WALLET_TRANSACTION_CREATE
    
    # This will help you to disable delete functionaliyt
    def has_delete_permission(self, request, obj=None):
        return config.CLIENT_WALLET_TRANSACTION_DELETE

    # This will help you to disable change functionality
    def has_change_permission(self, request, obj=None):
        return config.CLIENT_WALLET_TRANSACTION_UPDATE
        
    class Meta:
        model = ClientWalletTransaction
        
    search_fields = [
        'client__name',
        'wallet_reference',
        'transaction_details',
    ]
    list_display = (
        '__str__',
        'wallet_reference',
        'client_previous_wallet_amount',
        'transaction_amount',
        'transaction_details',
    )

admin.site.register(ClientWalletTransaction, ClientWalletTransactionAdmin)


class ClientIncorrectReferenceWalletTransactionAdmin(admin.ModelAdmin):
    
    def has_add_permission(self, request, obj=None):
        return False
    
    # This will help you to disable delete functionaliyt
    # def has_delete_permission(self, request, obj=None):
    #     return False

    # This will help you to disable change functionality
    def has_change_permission(self, request, obj=None):
        return False
        
    class Meta:
        model = ClientIncorrectReferenceWalletTransaction
        
    search_fields = [
        'payment_reference',
        'amount',
        'handled_by__username',
        'transaction_details'
    ]
    list_display = (
        '__str__',
        'amount',
        'transaction'
    )

admin.site.register(ClientIncorrectReferenceWalletTransaction, ClientIncorrectReferenceWalletTransactionAdmin)