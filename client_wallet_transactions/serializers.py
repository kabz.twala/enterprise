from django.db import transaction
from django.utils import timezone
from django.contrib.sites.shortcuts import get_current_site

from rest_framework import serializers
from rest_framework.reverse import reverse as api_reverse

from client_wallet_transactions.models import ClientWalletTransaction, ClientIncorrectReferenceWalletTransaction


class ClientWalletTransactionSerializer(serializers.ModelSerializer):

    client_wallet_transaction_uri = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = ClientWalletTransaction
        fields = ('id', 'client', 'invoice', 'wallet_reference', 'client_previous_wallet_amount', 'transaction_amount', 'transaction', 'transaction_details', 'client_wallet_transaction_uri')
        read_only_fields = ('id', 'client', 'wallet_reference', 'client_previous_wallet_amount')
    
    def get_client_wallet_transaction_uri(self, obj):
        return self.context['request'].build_absolute_uri("/api/enterprise/client-wallet-transactions/{id}/".format(id=obj.id))
        # request = self.context.get('request')
        # return api_reverse('enterprise-client-wallet-transactions-detail', kwargs={'id': obj.id}, request=request) e.g. -list, -detail, python manage.py show_urls
                
    def create(self, validated_data):
        
        raise serializers.ValidationError('User not permitted to create wallet transactions')

    def update(self, instance, validated_data):

        raise serializers.ValidationError('User not permitted to update wallet transactions')


class ClientIncorrectReferenceWalletTransactionSerializer(serializers.ModelSerializer):

    client_wallet_transaction_uri = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = ClientIncorrectReferenceWalletTransaction
        fields = ('id', 'payment_reference', 'amount', 'transaction', 'transaction_details', 'handled_by', 'log_details', 'client_wallet_transaction_uri')
        read_only_fields = ('id', 'payment_reference', 'amount', 'details', 'handled_by')
    
    def get_client_wallet_transaction_uri(self, obj):
        return self.context['request'].build_absolute_uri("/api/enterprise/client-incorrect-wallet-transactions/{id}/".format(id=obj.id))
        # request = self.context.get('request')
        # return api_reverse('enterprise-client-wallet-transactions-detail', kwargs={'id': obj.id}, request=request) e.g. -list, -detail, python manage.py show_urls
                
    def create(self, validated_data):
        
        raise serializers.ValidationError('User not permitted to create transactions')

    def update(self, instance, validated_data):

        if not self.context['request'].user.is_superuser or not self.context['request'].user.is_staff:
            raise serializers.ValidationError('User not permitted to update transactions')
        
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
                
        instance.handled_by = self.context['request'].user
        instance.log_details = instance.log_details + '\n\n{} - {}, handled by {}. {}'.format(timezone.localtime(timezone.now()) , validated_data['log_details'], self.context['request'].user.id, self.context['request'].user.username )

        instance.save()
        
        return instance