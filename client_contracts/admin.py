from django.contrib import admin
from django.contrib.auth import get_user_model
from django.db.models import Q

from clients.models import Client
from client_contracts.models import ClientContract
from client_packages.models import ClientPackage

from constance import config


class ClientContractAdmin(admin.ModelAdmin):

    def has_add_permission(self, request, obj=None):
        return config.CLIENT_CONTRACT_CREATE
    
    # This will help you to disable delete functionaliyt
    def has_delete_permission(self, request, obj=None):
        return config.CLIENT_CONTRACT_DELETE

    # This will help you to disable change functionality
    def has_change_permission(self, request, obj=None):
        return config.CLIENT_CONTRACT_UPDATE

    search_fields = [
        'unique_api_id',
        'client',
        'package',
        'fcm_key',
        'inbound_email',
        'whatsapp_support',
    ]
    list_display = (
        '__str__',
        'client',
        'package',
        'contract_reference',
        'active',
        'start',
    )

    class Meta:
        model = ClientContract

    contract = None
    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.contract = obj
        return super(ClientContractAdmin, self).get_form(request, obj, **kwargs)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        '''
        make sure if contract exists, can only change packages in the same category
        '''

        if db_field.name == "creator":
            kwargs["queryset"] = get_user_model().objects.filter(id=request.user.id)
        
        if db_field.name == "client":
            client_list = request.user.client_priviledged_user.filter(accepted=True, active=True)
            
            if request.resolver_match.kwargs.get('pk'):
                client_list = list(client_list.filter(contract_id=request.resolver_match.kwargs.get('pk')).values_list('contract__client_id', flat=True).distinct())
                kwargs["queryset"] = Client.objects.filter(Q(id__in=client_list) | Q(creator=request.user)).distinct()
            else:
                client_list = list(client_list.values_list('contract__client_id', flat=True).distinct())
                kwargs["queryset"] = Client.objects.filter(Q(id__in=client_list) | Q(creator=request.user)).distinct()
            
        if self.contract:
            if db_field.name == "package":
                kwargs["queryset"] = ClientPackage.objects.filter(category=self.contract.package.category)
            
        return super(ClientContractAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

admin.site.register(ClientContract, ClientContractAdmin)
