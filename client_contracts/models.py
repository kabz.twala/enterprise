from django.db import models, transaction
from django.conf import settings
from django.utils import timezone
from django.core.exceptions import ValidationError

from datetime import timedelta

from constance import config

from versatileimagefield.fields import VersatileImageField, PPOIField
from softdelete.models import SoftDeleteObject

from enterprise.middleware.get_request import get_request
from client_utils.models import CreatedModifiedMixin, PermissionsMixin
from client_utils.views import random_generator
from client_utils.permissions import get_privileges, get_privileges_exists_check
from clients.models import Client
from client_packages.models import ClientPackage

import ast


def html_upload_to(instance, filename):
    return 'enterprise/client_contracts/{0}/html/{1}'.format(instance.client.name, filename)


def css_upload_to(instance, filename):
    return 'enterprise/client_contracts/{0}/css/{1}'.format(instance.client.name, filename)


def logo_upload_to(instance, filename):
    return 'enterprise/client_contracts/{0}/img/logo/{1}'.format(instance.client.name, filename)


class ClientContract(CreatedModifiedMixin, PermissionsMixin, SoftDeleteObject):

    client = models.ForeignKey(Client, null=True, blank=True, on_delete=models.SET_NULL)
    package = models.ForeignKey(ClientPackage, blank=True, null=True, on_delete=models.SET_NULL)

    # string unique identifier
    unique_api_id = models.CharField(max_length=64, null=True, blank=True, unique=True)

    contract_reference = models.CharField(max_length=64, null=True, blank=True, unique=True)

    creator = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL, related_name="client_contract_creator")

    logo = VersatileImageField(
        'Image',
        upload_to=logo_upload_to,
        ppoi_field='image_ppoi_1',
        null=True, blank=True
    )
    image_ppoi_1 = PPOIField()

    fav_icon = VersatileImageField(
        'Image',
        upload_to=logo_upload_to,
        ppoi_field='image_ppoi_2',
        null=True, blank=True
    )
    image_ppoi_2 = PPOIField()

    loading_spinner = VersatileImageField(
        'Image',
        upload_to=logo_upload_to,
        ppoi_field='image_ppoi_3',
        null=True, blank=True
    )
    image_ppoi_3 = PPOIField()

    css = models.FileField(upload_to=css_upload_to, null=True, blank=True)
    html = models.FileField(upload_to=html_upload_to, null=True, blank=True)
    
    base_url = models.URLField(null=True, blank=True)
    short_base_url = models.URLField(null=True, blank=True)

    landing = models.BooleanField(default=False)

    terms_link = models.URLField(null=True, blank=True, help_text="https://domain/legal/terms")
    policy_link = models.URLField(null=True, blank=True, help_text="https://domain/legal/policy")

    fcm_key = models.CharField(max_length=255, null=True, blank=True)

    # e.g. mailchimp incoming email
    inbound_email = models.EmailField(null=True, blank=True)

    # sms package provider details
    sms_provider_username = models.CharField(max_length=256, null=True, blank=True)
    sms_provider_api_token = models.CharField(max_length=256, null=True, blank=True)
    
    whatsapp_support = models.CharField(
        max_length=256,
        null=True,
        blank=True,
        help_text="Example 083 or 2783"
    )

    active = models.BooleanField(default=False)

    start = models.DateTimeField(blank=True, null=True)

    # give the client time before setting the package as inactive
    time_until_expiry_in_days = models.PositiveIntegerField(default=0)

    '''
    save previous instance/field
    '''
    def __init__(self, *args, **kwargs):
        super(ClientContract, self).__init__(*args, **kwargs)
        self.previous_client = self.client
        self.previous_package = self.package

    def __str__(self):
        return "{}. {}".format(self.id, self.client.name)

    def save(self, invoice=None, *args, **kwargs):
        
        from client_contracts_history.models import ClientContractHistory
        from client_invoices.models import ClientInvoice
        from client_priviledged_contract_users.models import ClientPriviledgedContractUsers
        
        with transaction.atomic():
            
            create_new_contract = False

            if not self.client:
                raise ValidationError("Client is required")

            if not self.package:
                raise ValidationError("Package is required")

            if self.id:
                if int(self.client.id) != int(self.previous_client.id):
                    raise ValidationError('Not permitted to change client')

                if self.previous_package.category != self.package.category:
                    raise ValidationError("Package has to be in the same category. Please create a new contract.")

            if not self.unique_api_id:
                
                while True:
                    unique_api_id = random_generator(length=config.CLIENT_API_LENGTH, letters=config.CLIENT_API_ALPHABETS, digits=config.CLIENT_API_DIGITS, punctuation=config.CLIENT_API_PUNCTUATION, exclude_characters=ast.literal_eval(config.CLIENT_API_EXCLUDED_CHARACTERS))
                    if not type(self).objects.filter(unique_api_id=unique_api_id).exists():
                        break

                self.unique_api_id = unique_api_id

                while True:
                    contract_reference = random_generator(length=9, letters=config.CLIENT_API_ALPHABETS, digits=config.CLIENT_API_DIGITS, punctuation=config.CLIENT_API_PUNCTUATION, exclude_characters=ast.literal_eval(config.CLIENT_API_EXCLUDED_CHARACTERS))
                    if not type(self).objects.filter(contract_reference=contract_reference).exists():
                        break

                self.contract_reference = contract_reference

                create_new_contract = True
               
                
            request = get_request()

            user = {id: 0}
            if request and request.user:
                user = request.user
                if self.creator is None:
                    self.creator = request.user
            
            previous_package_id = 0
            previous_contract_start = None
            previous_contract_end = None
            current_contract_start = None

            previous_contract = ClientContract.objects.filter(client=self.client, package__category=self.package.category)
            
            amount = self.package.get_price()

            try:
                
                previous_contract = previous_contract[0]
                
                '''
                make sure not to create new contract of the same category
                this helps maintain upgrades and downgrades
                '''
                self.id = previous_contract.id
                
                if previous_contract.package != self.package and invoice is None:
                    # then upgrage or downgrade

                    payment_due = amount 
                    if previous_contract.active:
                        payment_due = amount - previous_contract.remaining_contract_balance()
                    
                    
                    details = '{} - Contract package change from {}. {} to {}. {}.'.format(
                        timezone.localtime(timezone.now()), 
                        previous_contract.package.id, 
                        previous_contract.package.name, 
                        self.package.id,
                        self.package.name
                    )

                    '''
                    check invoice with same package exists
                    refund if payment made and cancel invoice
                    create new invoice
                    '''
                    
                    invoices = ClientInvoice.objects.filter(contract = self, status = 0)
                    
                    # notify client invoice was cancelled 
                    if invoices.exists():
                        recipients = list( ClientPriviledgedContractUsers.objects.filter(contract=self, accepted=True, active=True, can_read_contract_invoices=True).values_list('user_id', flat=True) )
                        
                        for invoice in invoices:
                            self.invoice_update_and_mail_notificaton(
                                recipients=recipients,
                                invoice=invoice,
                                details='Cancelled ' + details,
                                origin='ClientContract.save',
                                status=2,
                                subject='Cancelled: Invoice Ref #{}'.format(invoice.invoice_reference),
                                view_in_browser_url=request.build_absolute_uri('/api/enterprise/client-invoices/{}/'.format(invoice.id))
                            )

                    invoice = ClientInvoice.objects.create(
                        contract = self,
                        package = self.package,
                        total_charge = amount,
                        payable_amount = payment_due,
                        status = 0,
                        details=details
                    )

                    if payment_due < 0:
                        '''
                        automatically clear the expense
                        '''
                        previous_contract_start = previous_contract.start
                        previous_contract_end = previous_contract.end()
                        current_contract_start = timezone.now()

                        invoice.payable_amount = 0

                        self.invoice_update_and_mail_notificaton(
                            recipients=list( ClientPriviledgedContractUsers.objects.filter(contract=self, accepted=True, active=True, can_read_contract_invoices=True).values_list('user_id', flat=True) ),
                            invoice=invoice,
                            details='Refunded ' + details + " Changed without need for payment. Payment due ({}{}) is less than 0. It was refunded back into the client's wallet".format(config.CLIENT_CURRENCY_SYMBOL, abs(payment_due)),
                            origin='ClientContract.save',
                            status=1,
                            subject='Paid: New Package Active - {}. {} - #{}'.format(self.package.id, self.package.name, invoice.invoice_reference),
                            view_in_browser_url=request.build_absolute_uri('/api/enterprise/client-invoices/{}/'.format(invoice.id)),
                            amount=abs(payment_due),
                            wallet_transaction=2
                        )

                        self.start = None
                        self.active = True
                        
                    else:

                        if config.CLIENT_INVOICE_AUTO_PAYMENT:
                            '''
                            pay invoice
                            check if amount < 0: save the rest in wallet  
                            inform client
                            '''
                            if self.client.wallet >= payment_due:

                                previous_contract_start = previous_contract.start
                                previous_contract_end = previous_contract.end()
                                current_contract_start = timezone.now()

                                self.invoice_update_and_mail_notificaton(
                                    recipients=list( ClientPriviledgedContractUsers.objects.filter(contract=self, accepted=True, active=True, can_read_contract_invoices=True).values_list('user_id', flat=True) ),
                                    invoice=invoice,
                                    details='Paid ' + details + " Payment due ({}{}) was paid using wallet auto payment. It was taken from the client's wallet.".format(config.CLIENT_CURRENCY_SYMBOL, payment_due),
                                    origin='ClientContract.save',
                                    status=1,
                                    subject='Paid: New Package Active - {}. {} - #{}'.format(self.package.id, self.package.name, invoice.invoice_reference),
                                    view_in_browser_url=request.build_absolute_uri('/api/enterprise/client-invoices/{}/'.format(invoice.id)),
                                    amount=payment_due,
                                    wallet_transaction=1
                                )
                            
                                self.start = None
                                self.active = True

                            else:
                                '''
                                inform client
                                '''
                                self.invoice_update_and_mail_notificaton(
                                    recipients=list( ClientPriviledgedContractUsers.objects.filter(contract=self, accepted=True, active=True, can_read_contract_invoices=True).values_list('user_id', flat=True) ),
                                    invoice=invoice,
                                    details='Not paid ' + details + " You have opted for the {}. {} package. To activate the package, please pay the invoice. Payment due ({}{})".format(self.package.id, self.package.name, config.CLIENT_CURRENCY_SYMBOL, payment_due),
                                    origin='ClientContract.save',
                                    status=0,
                                    subject='New Package Invoice: {}. {} - #{}'.format(self.package.id, self.package.name, invoice.invoice_reference),
                                    view_in_browser_url=request.build_absolute_uri('/api/enterprise/client-invoices/{}/'.format(invoice.id))
                                )
                                if previous_contract.active:
                                    '''
                                    retain same package as before
                                    '''
                                    self.package = previous_contract.package

                        else:
                            '''
                            inform client
                            '''
                            self.invoice_update_and_mail_notificaton(
                                recipients=list( ClientPriviledgedContractUsers.objects.filter(contract=self, accepted=True, active=True, can_read_contract_invoices=True).values_list('user_id', flat=True) ),
                                invoice=invoice,
                                details='Not paid ' + details + " You have opted for the {}. {} package. To activate the package, please pay the invoice. Payment due ({}{})".format(self.package.id, self.package.name, config.CLIENT_CURRENCY_SYMBOL, payment_due),
                                origin='ClientContract.save',
                                status=0,
                                subject='New Package Invoice: {}. {} - #{}'.format(self.package.id, self.package.name, invoice.invoice_reference),
                                view_in_browser_url=request.build_absolute_uri('/api/enterprise/client-invoices/{}/'.format(invoice.id))
                            )
                            if previous_contract.active:
                                '''
                                retain same package as before
                                '''
                                self.package = previous_contract.package

                    previous_amount = previous_contract.package.get_price()

                    previous_package_id = previous_contract.package.id
                    details = 'Contract package was changed from {}. {} ({}{}) to {}. {} ({}{}) (id, name, price) by user: {}. time_until_expiry_in_days: {}'.format( 
                        previous_package_id, 
                        previous_contract.package.name, 
                        config.CLIENT_CURRENCY_SYMBOL, 
                        previous_amount, 
                        self.package.id, 
                        self.package.name, 
                        config.CLIENT_CURRENCY_SYMBOL, 
                        amount,
                        user.id,
                        self.time_until_expiry_in_days, 
                    )
                    
                else:
                    '''
                    Do not create new contract
                    '''
                    
                    try:
                        user.id
                    except:
                        user = self.creator

                    details = 'Contract updated by user: {}. Package id and name: {}. {} ({}{}). time_until_expiry_in_days: {}'.format( 
                        user.id,
                        self.package.id,
                        self.package.name,
                        config.CLIENT_CURRENCY_SYMBOL, 
                        amount,
                        self.time_until_expiry_in_days, 
                    )

            except Exception as e:
                # print(e)
                # contract does not exist
                pass
            

            super(ClientContract, self).save(*args, **kwargs)
            
            if create_new_contract:
                '''
                create priviledge for contract creator
                '''
                ClientPriviledgedContractUsers.objects.create(
                    user=self.creator, contract=self, granted_by=self.creator, email=self.creator.email, accepted=True, accepted_date=timezone.now(), active=True, can_invite_other_contract_users=True, can_invite_other_contract_users_with_permissions=True, can_create_contract=True, can_update_contract=True, can_delete_contract=True, can_read_contract_history=True, can_read_contract_invoices=True, can_read_wallet_transactions=True,  can_read_other_contract_users=True, can_update_other_contract_users=True, can_delete_other_contract_users=True, details='{} - Created contract'.format( timezone.localtime(timezone.now()) )
                )
                if self.client.creator != self.creator:
                    '''
                    create priviledge for client creator
                    '''
                    ClientPriviledgedContractUsers.objects.create(
                        user=self.client.creator, contract=self, granted_by=self.creator, email=self.client.creator.email, accepted=True, accepted_date=timezone.now(), active=True, can_invite_other_contract_users=True, can_invite_other_contract_users_with_permissions=True, can_create_contract=True, can_update_contract=True, can_delete_contract=True, can_read_contract_history=True, can_read_contract_invoices=True, can_read_wallet_transactions=True,  can_read_other_contract_users=True, can_update_other_contract_users=True, can_delete_other_contract_users=True, details='{} - Created contract'.format( timezone.localtime(timezone.now()) )
                    )
                

                details = '{} - New contract created. Not active yet. Package name: {} ({}{})'.format(
                    timezone.localtime(timezone.now()),
                    self.package.name,
                    config.CLIENT_CURRENCY_SYMBOL, 
                    amount
                )

                payment_due = amount

                invoice = ClientInvoice.objects.create(
                    contract = self,
                    package = self.package,
                    total_charge = amount,
                    payable_amount = payment_due,
                    status = 0,
                    details = details
                )

                if config.CLIENT_INVOICE_AUTO_PAYMENT:
                    '''
                    pay invoice
                    check if amount < 0: save the rest in wallet  
                    inform client
                    '''
                    if self.client.wallet >= payment_due:

                        current_contract_start = timezone.now()

                        self.invoice_update_and_mail_notificaton(
                            recipients=list( ClientPriviledgedContractUsers.objects.filter(contract=self, accepted=True, active=True, can_read_contract_invoices=True).values_list('user_id', flat=True) ),
                            invoice=invoice,
                            details='Paid ' + details + " Payment due ({}{}) was paid using wallet auto payment. It was taken from the client's wallet.".format(config.CLIENT_CURRENCY_SYMBOL, payment_due),
                            origin='ClientContract.save',
                            status=1,
                            subject='Paid: New Package Active - {}. {} - #{}'.format(self.package.id, self.package.name, invoice.invoice_reference),
                            view_in_browser_url=request.build_absolute_uri('/api/enterprise/client-invoices/{}/'.format(invoice.id)),
                            amount=payment_due,
                            wallet_transaction=1
                        )
                    
                        self.start = None
                        self.active = True

                    else:
                        '''
                        inform client
                        retain same page as before
                        '''
                        
                        self.invoice_update_and_mail_notificaton(
                            recipients=list( ClientPriviledgedContractUsers.objects.filter(contract=self, accepted=True, active=True, can_read_contract_invoices=True).values_list('user_id', flat=True) ),
                            invoice=invoice,
                            details='Not paid ' + details + " You have opted for the {}. {} package. To activate the package, please pay the invoice. Payment due ({}{})".format(self.package.id, self.package.name, config.CLIENT_CURRENCY_SYMBOL, payment_due),
                            origin='ClientContract.save',
                            status=0,
                            subject='New Package Invoice: {}. {} - #{}'.format(self.package.id, self.package.name, invoice.invoice_reference),
                            view_in_browser_url=request.build_absolute_uri('/api/enterprise/client-invoices/{}/'.format(invoice.id))
                        )

                else:
                    '''
                    inform client
                    retain same page as before
                    '''

                    self.invoice_update_and_mail_notificaton(
                        recipients=list( ClientPriviledgedContractUsers.objects.filter(contract=self, accepted=True, active=True, can_read_contract_invoices=True).values_list('user_id', flat=True) ),
                        invoice=invoice,
                        details='Not paid ' + details + " You have opted for the {}. {} package. To activate the package, please pay the invoice. Payment due ({}{})".format(self.package.id, self.package.name, config.CLIENT_CURRENCY_SYMBOL, payment_due),
                        origin='ClientContract.save',
                        status=0,
                        subject='New Package Invoice: {}. {} - #{}'.format(self.package.id, self.package.name, invoice.invoice_reference),
                        view_in_browser_url=request.build_absolute_uri('/api/enterprise/client-invoices/{}/'.format(invoice.id))
                    )
                    
            try:
                client_contract_history = ClientContractHistory.objects.get(contract=self, creator=user, previous_package_id=previous_package_id, current_package_id=self.package.id, details=details, previous_contract_start=previous_contract_start, previous_contract_end=previous_contract_end, current_contract_start=current_contract_start)
            except:
                client_contract_history = ClientContractHistory.objects.create(contract=self, creator=user, previous_package_id=previous_package_id, current_package_id=self.package.id, details=details, previous_contract_start=previous_contract_start, previous_contract_end=previous_contract_end, current_contract_start=current_contract_start)

            
            if self.active and self.start is None:
                self.start = timezone.now()

                client_contract_history.current_contract_end = self.end()
                client_contract_history.details =  client_contract_history.details + '\n\n{} - Contract is now active'.format( timezone.localtime(timezone.now()) )
                client_contract_history.save()

                invoice.details = invoice.details + '\n\n{} - Contract is now active'.format( timezone.localtime(timezone.now()) )
                invoice.save()
                self.save()
                    

    def invoice_update_and_mail_notificaton(self, recipients=[], invoice=None, details='', status=0, origin='', subject='', view_in_browser_url='', amount=0, wallet_transaction=None):

        _invoice = False
        invoice_reference = ''
        invoice_date = ''

        if invoice:
            _invoice = True

            # invoice.status=status
            # invoice.details=details
            # invoice.save()

            invoice_reference = invoice.invoice_reference
            invoice_date = invoice.created.strftime("%B %d, %Y %H:%M:%S")

            invoice.make_invoice_payment(
                origin=origin, 
                details=details,
                amount=amount,
                status = status,
                wallet_transaction=wallet_transaction
            )
            
            image_icon = self.package.image_icon.thumbnail

            packages = [{'name': self.package.name, 'image_icon': image_icon, 'description': self.package.description, 'price': amount, 'features': self.package.get_features()}]
        
        self.client.send_email(
            packages=packages,
            amount=amount,
            tax_or_vat_price=self.package.tax_or_vat_price,
            origin=origin, 
            invoice=_invoice,
            invoice_reference=invoice_reference,
            invoice_date=invoice_date,
            recipients=recipients,
            details=details,
            status=status,
            subject=subject, 
            message_body=details,
            view_in_browser_url=view_in_browser_url
        )


    @classmethod
    def get_queryset(cls, user):

        if not user.is_authenticated:
            return ClientContract.objects.none()

        if user.is_superuser or user.is_staff:
            return ClientContract.objects.all()

        contracts = list( user.client_priviledged_user.filter(accepted=True, active=True).values_list('contract_id', flat=True) )
        return ClientContract.objects.filter(id__in=contracts)

    def end(self):
        '''
        Returns the datetime in which the contract ends. Considers contract start date plus the duration the package is granted. 
        Excludes time_until_expiry_in_days
        '''
        try:
            _end = self.start + timedelta(days=self.package.duration_in_days)
            return _end.astimezone()
        except Exception as e:
            return None

    def days_remaining(self):
        '''
        Returns an integer of the days until the contract expires. 
        time_until_expiry_in_days is also included
        '''
        try:
            date = self.end() + timedelta(days=self.time_until_expiry_in_days) - timezone.now()
            
            if date.days >= 0:
                return date.days + 1
            
            return 0

        except Exception as e:
            return 0
    
    def remaining_contract_balance(self):
        '''
        Remaing contract balance: 
        package_price - [package_rate_per_day * (current_day - contract_start_day)]
        '''
        try:
            amount = self.package.get_price()

            balance = amount - round(self.package.rate_per_day() * (timezone.now() - self.start).days, 2)
            if balance < 0:
                return 0
            return balance
        except Exception as e:
            return 0
    
    @classmethod
    def has_permission(cls, request):
        """ 
        Permissions for Privileged users.
        """

        if request.user.is_superuser or request.user.is_staff:
            return True

        return get_privileges_exists_check(request.user) 

    def has_object_permission(self, request, obj):
        """ 
        Object Permissions for Privileged users.
        """

        if request.user.is_superuser or request.user.is_staff:
            return True

        privileges = get_privileges(request.user) 

        if privileges:

            if request.method == 'GET':
                if obj:
                    return privileges.filter(contract_id=obj.id).exists()
                return privileges.exists()
            elif request.method == 'POST':
                return privileges.filter(contract_id=obj.id, can_create_contract=True).exists()
            elif request.method == 'PUT':
                return privileges.filter(contract_id=obj.id, can_update_contract=True).exists()
            elif request.method == 'DELETE':
                return privileges.filter(contract_id=obj.id, can_delete_contract=True).exists()
            elif request.method == 'PATCH':
                return privileges.filter(contract_id=obj.id, can_update_contract=True).exists()
        
        return False
        