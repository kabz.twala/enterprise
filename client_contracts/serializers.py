from django.db import transaction
from django.db.models import Q

from rest_framework import serializers
from rest_framework.reverse import reverse as api_reverse

from versatileimagefield.serializers import VersatileImageFieldSerializer

from clients.models import Client
from client_contracts.models import ClientContract
from client_packages.models import ClientPackage


# try use for permissions on individual users
class UserFilteredSlugRelatedField(serializers.SlugRelatedField):
    def get_queryset(self):
        request = self.context.get('request', None)
        queryset = super(UserFilteredSlugRelatedField, self).get_queryset()
        
        if not request or not queryset:
            return None

        client_list = request.user.client_priviledged_user.filter(accepted=True, active=True)
        
        if request.resolver_match.kwargs.get('pk'):
            client_list = list(client_list.filter(contract_id=request.resolver_match.kwargs.get('pk')).values_list('contract__client_id', flat=True).distinct())
            return queryset.filter(Q(id__in=client_list) | Q(creator=request.user)).distinct()
        else:
            client_list = list(client_list.values_list('contract__client_id', flat=True).distinct())
            return queryset.filter(Q(id__in=client_list) | Q(creator=request.user)).distinct()


class PackageFilteredSlugRelatedField(serializers.SlugRelatedField):

    def get_queryset(self):
        request = self.context.get('request', None)
        queryset = super(PackageFilteredSlugRelatedField, self).get_queryset()

        if not request or not queryset:
            return None

        try:
            contract = ClientContract.objects.get(id=request.resolver_match.kwargs.get('pk'))
            return queryset.filter(category=contract.package.category)
        except:
            return queryset.all()


class ClientContractSerializer(serializers.ModelSerializer):

    logo = VersatileImageFieldSerializer(
        sizes='product_headshot',
        required=False,
    )
    fav_icon = VersatileImageFieldSerializer(
        sizes='product_headshot',
        required=False,
    )
    loading_spinner = VersatileImageFieldSerializer(
        sizes='product_headshot',
        required=False,
    )

    client_contract_uri = serializers.SerializerMethodField(read_only=True)
    rate_per_day = serializers.SerializerMethodField(read_only=True)
    client = UserFilteredSlugRelatedField(many=False, slug_field="name", queryset=Client.objects)
    package = PackageFilteredSlugRelatedField(many=False, slug_field="name", queryset=ClientPackage.objects)

    class Meta:
        model = ClientContract
        fields = ('id', 'client', 'package', 'contract_reference', 'creator', 'logo', 'fav_icon', 'loading_spinner', 'css', 'html', 'base_url', 'short_base_url', 'landing', 'terms_link', 'policy_link', 'fcm_key', 'inbound_email', 'sms_provider_username', 'sms_provider_api_token', 'whatsapp_support', 'active', 'deleted', 'start', 'end', 'time_until_expiry_in_days', 'days_remaining', 'rate_per_day', 'remaining_contract_balance', 'client_contract_uri')
        read_only_fields = ('id', 'contract_reference', 'creator', 'active', 'deleted', 'start')
    

    def get_client_contract_uri(self, obj):
        # return self.context['request'].build_absolute_uri("/api/enterprise/client-contracts/{id}/".format(id=obj.id))
        request = self.context.get('request')
        return api_reverse('enterprise-client-contract-detail', kwargs={'pk': obj.id}, request=request) # e.g. -list, -detail, python manage.py show_urls
                
    def get_rate_per_day(self, obj):
        return obj.package.rate_per_day()

    def create(self, validated_data):
        
        user = self.context['request'].user
        if user.is_authenticated:

            with transaction.atomic():
                validated_data['creator'] = user
                try:
                    instance = ClientContract.objects.create(**validated_data)
                except Exception as e:
                    raise serializers.ValidationError(str(e))
                
                return instance
            
        raise serializers.ValidationError('User needs to be signed in')

    def update(self, instance, validated_data):

        user = self.context['request'].user
        if user.is_authenticated:

            try:

                client = Client.objects.get(name=self.context['request'].data.get('client'))
            
                if int(client.id) != int(instance.client.id):
                    raise serializers.ValidationError('Not permitted to change client')

                try:
                    package = ClientPackage.objects.get(name=self.context['request'].data.get('package'))
                except:
                    package = None

                if package is None:
                    raise serializers.ValidationError('Package is required')
                elif package.category.id != instance.package.category.id:
                    raise serializers.ValidationError('Package needs to be in the same category as previously selected package')
                    
                if validated_data['css'] == None:
                    validated_data['css'] = instance.css
                if validated_data['html'] == None:
                    validated_data['html'] = instance.html

                for attr, value in validated_data.items():
                    setattr(instance, attr, value)

                instance.save()
        
                return instance

            except Exception as e:
                print(e)
                raise serializers.ValidationError(str(e))
            
        raise serializers.ValidationError('User needs to be signed in')
