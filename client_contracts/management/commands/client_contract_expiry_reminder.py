from django.core.management.base import BaseCommand

from client_utils.tasks import send_client_contract_expiry_reminder

from constance import config


class Command(BaseCommand):
    # python manage.py send_to_shared_drive
    
    help = 'Email clients reminding them to pay before their contract expires. e.g. >>> python manage.py client_contract_expiry_reminder'

    def handle(self, *args, **kwargs):

        send_client_contract_expiry_reminder.apply_async(queue='long_request', priority=config.CELERY_LOW_PRIORITY)

        self.stdout.write('Initiating reminder mail...')


    