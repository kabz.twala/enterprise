from rest_framework import viewsets
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated

from client_contracts.models import ClientContract
from client_contracts.serializers import ClientContractSerializer
from client_utils.permissions import ModelBasedPermissions
from client_utils.pagination import ClientCustomPagination


class ClientContractPermissions(ModelBasedPermissions):
    model = ClientContract
    message = "You don't have permission to access contracts"


class ClientContractViewSet(viewsets.ModelViewSet):

    model = ClientContract
    serializer_class = ClientContractSerializer
    permission_classes = (IsAuthenticated, ClientContractPermissions)
    pagination_class = ClientCustomPagination
    search_fields = (
        'unique_api_id',
        'client',
        'package',
        'fcm_key',
        'inbound_email',
        'whatsapp_support',
    )
    ordering_fields = (
        'id',
        'active',
        'start',
        'end',
        'created',
        'modified'
    )

    def get_queryset(self):
        
        if not self.request.user.is_authenticated:
            raise PermissionDenied()

        queryset = ClientContract.get_queryset(self.request.user)

        if queryset is not None:
            return queryset.order_by('-modified')

        raise PermissionDenied()


