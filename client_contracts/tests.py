from threading import current_thread

from django.test import TestCase, RequestFactory
from django.contrib.auth.models import User
from django.urls import reverse

from client_package_categories.models import ClientPackageCategory
from client_packages.models import ClientPackage
from clients.models import Client
from client_contracts.models import ClientContract
from enterprise.middleware import get_request


def create_contract(user):

    category = ClientPackageCategory.objects.create(title='Payment Gateway')
    package = ClientPackage.objects.create(name = "Card payments", active = True, category = category, duration_in_days = 10, use_feature_total = False, price = 100, tax_or_vat_price = 0, description = "")

    client = Client.objects.create(name='Client 1', creator=user)

    return ClientContract.objects.create(client= client, package=package, creator=user)


class ClientContractTest(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        
    def test_create_contract_valid_api_id(self):

        user = User.objects.create_user(username='Siv', password='password', is_active=True, email='your@email.com')
        self.client.login(username=user.username, password='password')

        request = self.factory.get(reverse('enterprise-client-contract-list'))
        request.user = user
        # mimics get_request middleware
        get_request._requests[current_thread()] = request

        contract = create_contract(user)

        self.assertTrue(contract.unique_api_id)