from django.apps import AppConfig


class ClientContractsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'client_contracts'
