from __future__ import absolute_import

"""
Implementation from http://celery.readthedocs.org/en/latest/django/first-steps-with-django.html
"""
import os

from celery import Celery
from celery.schedules import crontab
from django.conf import settings

# set the default Django settings module for the 'celery' program
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'enterprise.settings')

app = Celery('enterprise')

# Using a string here means the worker will not have
# to pickle the object when using Windows.
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))

#Start Celery From Terminal
#celery -A enterprise worker -l info
