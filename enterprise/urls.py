"""enterprise URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django import get_version
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin

from rest_framework import routers
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
)

from client_contracts import views as client_contract_views
from client_contracts_history import views as client_contract_history_views
from client_event_logs import views as client_event_log_views
from client_invoices import views as client_invoice_views
from client_packages import views as client_package_views
from client_package_categories import views as client_package_categories_views
from client_priviledged_contract_users import views as client_priviledged_contract_user_views
from client_sites import views as client_sites_views
from client_wallet_transactions import views as client_wallet_transaction_views
from clients import views as client_views

from distutils.version import StrictVersion


api_enterprise_router = routers.DefaultRouter()

api_enterprise_router.register(r'client-contracts', client_contract_views.ClientContractViewSet, basename='enterprise-client-contract')
api_enterprise_router.register(r'client-contract-history', client_contract_history_views.ClientContractHistoryViewSet, basename='enterprise-client-contract-history')
api_enterprise_router.register(r'client-event-logs', client_event_log_views.ClientEventLogViewSet, basename='enterprise-client-event-logs')
api_enterprise_router.register(r'client-invoices', client_invoice_views.ClientInvoiceViewSet, basename='enterprise-client-invoices')
api_enterprise_router.register(r'client-packages', client_package_views.ClientPackageViewSet, basename='enterprise-client-packages')
api_enterprise_router.register(r'client-package-categories', client_package_categories_views.ClientPackageCategoryViewSet, basename='enterprise-client-package-categories')
api_enterprise_router.register(r'client-priviledged-contract-users', client_priviledged_contract_user_views.ClientPriviledgedContractUsersViewSet, basename='enterprise-client-priviledged-contract-users')
api_enterprise_router.register(r'client-sites', client_sites_views.ClientSiteViewSet, basename='enterprise-client-sites')
api_enterprise_router.register(r'client-wallet-transactions', client_wallet_transaction_views.ClientWalletTransactionViewSet, basename='enterprise-client-wallet-transactions')
api_enterprise_router.register(r'client-incorrect-wallet-transactions', client_wallet_transaction_views.ClientIncorrectWalletTransactionViewSet, basename='enterprise-client-incorrect-wallet-transactions')
api_enterprise_router.register(r'clients', client_views.ClientViewSet, basename='enterprise-clients')

if StrictVersion(get_version()) < StrictVersion('2.0'):

    from django.conf.urls import url, include
    
    urlpatterns = [
        url(r'^admin/', admin.site.urls),
        
        url(r'^api/enterprise/', include(api_enterprise_router.urls)),
        url(r'^api-auth/', include('rest_framework.urls')),

        url(r'^api/token/$', TokenObtainPairView.as_view(), name='token_obtain_pair'),

        url(r'^api/enterprise/clients-login/$', client_views.ClientLoginViewSet.as_view(), name='enterprise-clients-login'),

        url(r'^api/enterprise/client-accept-contract-invite/(?P<pk>\d+)/(?P<url>\w+)/$', client_priviledged_contract_user_views.ClientAcceptContractInviteViewSet.as_view(), name='enterprise-client-accept-contract-invite'),
        url(r'^api/enterprise/client-accepted-contract-invite/(?P<pk>\d+)/(?P<url>\w+)/$', client_priviledged_contract_user_views.ClientAcceptedContractInviteViewSet.as_view(), name='enterprise-client-accepted-contract-invite'),
    ]

else:
    
    from django.urls import include, path

    urlpatterns = [
        path('admin/', admin.site.urls),

        path('api/enterprise/', include(api_enterprise_router.urls)),
        path('api-auth/', include('rest_framework.urls')),

        path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),

        path('api/enterprise/clients-login/', client_views.ClientLoginViewSet.as_view(), name='enterprise-clients-login'),

        path('api/enterprise/client-accept-contract-invite/<int:pk>/<str:url>/', client_priviledged_contract_user_views.ClientAcceptContractInviteViewSet.as_view(), name='enterprise-client-accept-contract-invite'),
        path('api/enterprise/client-accepted-contract-invite/<int:pk>/<str:url>/', client_priviledged_contract_user_views.ClientAcceptedContractInviteViewSet.as_view(), name='enterprise-client-accepted-contract-invite'),
    ]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
