from django.db import transaction

from rest_framework import serializers

from django.contrib.sites.models import Site


class ClientSiteSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Site
        fields = ('id', 'domain', 'name')
        read_only_fields = ('id',)
    
    def validate_name(self, value):

        if len(value) == 0:
            raise serializers.ValidationError("Name is required")
        else:
            try:
                Site.objects.get(name=value)
                raise serializers.ValidationError("Name is already in use")
            except:
                pass

        return value

    def validate_domain(self, value):

        if len(value) == 0:
            raise serializers.ValidationError("Domain is required")
        else:
            try:
                Site.objects.get(domain=value)
                raise serializers.ValidationError("Domain is already in use")
            except:
                pass

        return value
                
    def create(self, validated_data):
            
        user = self.context['request'].user
        if user.is_authenticated:

            if not user.is_superuser or not user.is_staff:
                raise serializers.ValidationError('User needs is not permitted')

            with transaction.atomic():
                
                return Site.objects.create(**validated_data)
        
        raise serializers.ValidationError('User needs to be signed in')

    def update(self, instance, validated_data):

        user = self.context['request'].user
        if user.is_authenticated:

            if not user.is_superuser or not user.is_staff:
                raise serializers.ValidationError('User needs is not permitted')

            with transaction.atomic():

                instance.save()
                
                return instance
        
        raise serializers.ValidationError('User needs to be signed in')
