from django.apps import AppConfig


class ClientSitesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'client_sites'
