from http import HTTPStatus

from django.test import TestCase
from django.contrib.auth.models import User
from django.urls import reverse 


class SiteTest(TestCase):

    # should pass | superuser is logged in
    def test_create_site_valid(self, name='localhost:8000', domain='localhost:8000'):
        
        superuser = User.objects.create_superuser('admin', 'admin@myproject.com', 'password')
        self.client.login(username=superuser.username, password='password')

        response = self.client.post(reverse('enterprise-client-sites-list'), data={'name': name, 'domain': domain})
        
        self.assertEqual(response.status_code, HTTPStatus.CREATED)

    # should fail | superuser is not logged in
    def test_create_site_invalid(self, name='localhost:8000', domain='localhost:8000'):
        
        response = self.client.post(reverse('enterprise-client-sites-list'), data={'name': name, 'domain': domain})
        
        self.assertEqual(response.status_code, HTTPStatus.UNAUTHORIZED)

