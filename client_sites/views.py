from django.http import JsonResponse
from django.contrib.sites.models import Site

from rest_framework import decorators, status, viewsets
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated

from client_sites.serializers import ClientSiteSerializer
from client_utils.permissions import IsStaffOrSuperuser
from client_utils.views import set_config_value


class ClientSiteViewSet(viewsets.ModelViewSet):

    model = Site
    serializer_class = ClientSiteSerializer
    permission_classes = (IsAuthenticated, IsStaffOrSuperuser)
    search_fields = (
        'name',
        'domain',
    )
    ordering_fields = (
        'name',
        'domain',
    )

    def get_queryset(self):
        
        if not self.request.user.is_authenticated:
            raise PermissionDenied()
        
        if not self.request.user.is_superuser or not self.request.user.is_staff:
            raise PermissionDenied('User is not permitted')

        queryset = None

        if self.request.user.is_superuser or self.request.user.is_staff:
            queryset = Site.objects.all()

        if queryset is not None:
            return queryset

        raise PermissionDenied()

    @decorators.action(detail=False, methods=['get'], permission_classes = (IsAuthenticated, IsStaffOrSuperuser))
    def set_client_url(self, request):

        try:
            site = request.build_absolute_uri('/')

            set_config_value('SITE_URL', site)

            return JsonResponse({'detail': 'SITE_URL updated to {}'.format(site)}, status=status.HTTP_200_OK)

        except Exception as e:
            
            return JsonResponse({'detail': '{}'.format(e)}, status=status.HTTP_400_BAD_REQUEST)