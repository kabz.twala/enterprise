from collections import namedtuple
from constance import config

from django.db import models
from django.core.exceptions import MultipleObjectsReturned
from django.db.models import Value
from django.db.models.functions import Concat

from softdelete.models import SoftDeleteObject

from enterprise.middleware.get_request import get_request
from client_contracts.models import ClientContract
from client_utils.models import CreatedModifiedMixin, PermissionsMixin
from client_utils.views import get_field_choices, random_generator
from client_utils.permissions import get_privileges, get_privileges_exists_check

import ast

from client_packages.models import ClientPackage


class ClientInvoice(CreatedModifiedMixin, PermissionsMixin, SoftDeleteObject):

    contract = models.ForeignKey(ClientContract, blank=True, null=True, on_delete=models.SET_NULL)
    package = models.ForeignKey(ClientPackage, blank=True, null=True, on_delete=models.SET_NULL)

    invoice_reference = models.CharField(max_length=255, unique=True, blank=False, null=False)

    total_charge = models.DecimalField(max_digits=8, decimal_places=2, null=False, blank=False)
    payable_amount = models.DecimalField(max_digits=8, decimal_places=2, null=False, blank=False)
    
    _STATUS_TYPE = {
        0: 'Unpaid',
        1: 'Paid',
        2: 'Cancelled'
    }
    status_choices = (
        'unpaid',
        'paid',
        'cancelled',
    )
    STATUS_TYPE = namedtuple('STATUS_TYPE', status_choices)(*range(0, len(status_choices)))
    status = models.PositiveIntegerField(default=0, choices=get_field_choices(STATUS_TYPE))

    details = models.TextField(default='', blank=True, null=True)

    origin = models.TextField(blank=True, null=True)

    def __str__(self):
        return "{}. {}".format(self.id, self.invoice_reference)

    def save(self, *args, **kwargs):

        if not self.invoice_reference:
            while True:
                invoice_reference = random_generator(length=config.CLIENT_INVOICE_LENGTH, letters=config.CLIENT_INVOICE_ALPHABETS, digits=config.CLIENT_INVOICE_DIGITS, punctuation=config.CLIENT_INVOICE_PUNCTUATION, exclude_characters=ast.literal_eval(config.CLIENT_INVOICE_EXCLUDED_CHARACTERS))
                if not type(self).objects.filter(invoice_reference=invoice_reference).exists():
                    break
            self.invoice_reference = invoice_reference
            
        return super(ClientInvoice, self).save(*args, **kwargs)

    @classmethod
    def get_queryset(cls, user):

        if not user.is_authenticated:
            return ClientInvoice.objects.none()

        if user.is_superuser or user.is_staff:
            return ClientInvoice.objects.all()

        contracts = list( user.client_priviledged_user.filter(accepted=True, active=True, can_read_contract_invoices=True).values_list('contract_id', flat=True) )
        return ClientInvoice.objects.filter(contract_id__in=contracts)
    
    def get_or_create_invoice(contract, payment_due, details=''):

        try:
            invoice = ClientInvoice.objects.get(contract = contract, package=contract.package, status = 0)

        except MultipleObjectsReturned:
            invoices = ClientInvoice.objects.filter(contract = contract, package=contract.package, status = 0)

            invoice = invoices[0]

            # cancel
            invoices.update(status=2, details=Concat('details', Value('\n\n{} - Cancelled: MultipleObjectsReturned - Represented by id={}'.format(timezone.localtime(timezone.now()), invoice.id)))).exclude(id=invoice.id)
            
        except Exception as e:
            print(e)
            invoice = ClientInvoice.objects.create(
                contract = contract,
                package = contract.package,
                total_charge = contract.package.price,
                payable_amount = payment_due,
                status = 0,
                details = details
            )

        return invoice

    def make_invoice_payment(self, origin=None, details='', amount=0, status=1, wallet_transaction=None, send_mail=False, subject=''):
        
        from client_wallet_transactions.models import ClientWalletTransaction

        if send_mail:
            '''
            send message/email
            '''
            from client_priviledged_contract_users.models import ClientPriviledgedContractUsers

            self.contract.invoice_update_and_mail_notificaton(
                recipients=list( ClientPriviledgedContractUsers.objects.filter(contract=self.contract, accepted=True, active=True, can_read_contract_invoices=True).values_list('user_id', flat=True) ),
                invoice=self,
                details=details,
                origin=origin,
                status=status,
                subject=subject,
                view_in_browser_url=get_request().build_absolute_uri('/api/enterprise/client-invoices/{}/'.format(self.id)),
                wallet_transaction=wallet_transaction,
                amount=amount
            )
        
        else:

            self.origin = origin
            self.details = self.details + '\n\n' + details
            self.status = status
            self.save()

            if wallet_transaction:  

                ClientWalletTransaction.objects.create(
                    client=self.contract.client, 
                    invoice=self, 
                    client_previous_wallet_amount=self.contract.client.wallet,
                    transaction_amount=amount,
                    transaction=wallet_transaction, 
                    transaction_details=details
                )
    
    @classmethod
    def has_permission(cls, request):
        """ 
        Permissions for Privileged users.
        """

        if request.user.is_superuser or request.user.is_staff:
            return True

        return get_privileges_exists_check(request.user) 

    def has_object_permission(self, request, obj):
        """ 
        Object Permissions for Privileged users.
        """

        if request.user.is_superuser or request.user.is_staff:
            return True

        privileges = get_privileges(request.user) 

        if privileges:

            if request.method == 'GET':
                if obj:
                    return privileges.filter(contract_id=obj.contract.id, can_read_contract_invoices=True).exists()
                return privileges.filter(can_read_contract_invoices=True).exists()
        
        return False
