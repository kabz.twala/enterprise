from urllib import request
from django.db import transaction

from constance import config
from rest_framework import serializers
from rest_framework.reverse import reverse as api_reverse

from client_invoices.models import ClientInvoice

class ClientInvoiceSerializer(serializers.ModelSerializer):
    
    client_invoice_uri = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = ClientInvoice
        fields = ('id', 'contract', 'package', 'invoice_reference', 'total_charge', 'payable_amount', 'status', 'details', 'client_invoice_uri')
        read_only_fields = ('id', 'contract', 'package', 'invoice_reference', 'total_charge', 'payable_amount', 'status', 'details')
    
    def get_client_invoice_uri(self, obj):
        return self.context['request'].build_absolute_uri("/api/enterprise/client-invoices/{id}/".format(id=obj.id))
        # request = self.context.get('request')
        # return api_reverse('enterprise-invoices-detail', kwargs={'id': obj.id}, request=request) e.g. -list, -detail, python manage.py show_urls
                
    def create(self, validated_data):
        
        raise serializers.ValidationError('User not permitted to create an invoice')

    def update(self, instance, validated_data):

        raise serializers.ValidationError('User not permitted to update an invoice')
