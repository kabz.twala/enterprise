from django.db import transaction
from django.utils import timezone

from constance import config

from rest_framework import decorators, status, viewsets
from rest_framework.response import Response
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated

from client_invoices.models import ClientInvoice
from client_invoices.serializers import ClientInvoiceSerializer
from client_utils.permissions import ModelBasedPermissions
from client_utils.pagination import ClientCustomPagination


class ClientInvoicePermissions(ModelBasedPermissions):
    model = ClientInvoice
    message = "You don't have permission to access invoices"


class ClientInvoiceViewSet(viewsets.ModelViewSet):

    model = ClientInvoice
    serializer_class = ClientInvoiceSerializer
    permission_classes = (IsAuthenticated, ClientInvoicePermissions)
    pagination_class = ClientCustomPagination
    search_fields = (
        'invoice_reference',
        'contract__client__name',
    )
    ordering_fields = (
        'id',
        'status',
        'created',
        'modified'
    )

    def get_queryset(self):
        if not self.request.user.is_authenticated:
            raise PermissionDenied()

        queryset = ClientInvoice.get_queryset(self.request.user)

        if queryset is not None:
            return queryset.order_by('-modified')

        raise PermissionDenied()


    @decorators.action(detail=False, methods=['post'], permission_classes = (IsAuthenticated, ClientInvoicePermissions), url_path="make-invoice-update")
    def make_invoice_update(self, request):
        
        try:
            with transaction.atomic():
                invoice_reference = request.data.get('invoice_reference')
                status_ = request.data.get('status')
                details = request.data.get('details')
                origin = 'ClientInvoiceViewSet.make_invoice_update'

                invoice = ClientInvoice.objects.get(invoice_reference=invoice_reference)
                if int(invoice.status) == 2:
                    return Response({'detail': 'Invoice was cancelled on {}'.format(timezone.localtime(invoice.modified))}, status=status.HTTP_400_BAD_REQUEST)
                elif int(invoice.status) == 1:
                    return Response({'detail': 'Invoice was paid on {}'.format(timezone.localtime(invoice.modified))}, status=status.HTTP_400_BAD_REQUEST)

                if int(status_) == 1:

                    if invoice.contract.client.wallet < invoice.payable_amount:
                        return Response({'detail': 'Money in the Client\'s wallet ({}{}) is less than amount due ({}{})'.format(config.CLIENT_CURRENCY_SYMBOL, invoice.contract.client.wallet, config.CLIENT_CURRENCY_SYMBOL, invoice.payable_amount)}, status=status.HTTP_400_BAD_REQUEST)
                    
                    invoice.contract.start = None
                    invoice.contract.active = True
                    invoice.contract.package = invoice.package
                    invoice.contract.save(invoice=invoice)
                    
                    invoice.make_invoice_payment(
                        origin=origin, 
                        details="{} - Paid -  Payment due ({}{}) was paid. It was taken from the client's wallet.".format(timezone.localtime(timezone.now()), config.CLIENT_CURRENCY_SYMBOL, invoice.payable_amount), 
                        amount=invoice.payable_amount, 
                        status=status_, 
                        wallet_transaction=1, 
                        send_mail=True,
                        subject='Paid: {}. {} - #{}'.format(invoice.package.id, invoice.package.name, invoice.invoice_reference)
                    )

                elif int(status_) == 2:

                    invoice.make_invoice_payment(
                        origin=origin, 
                        details="{} - Cancelled: {}".format(timezone.localtime(timezone.now()), details),
                        status=status_, 
                        send_mail=True,
                        subject='Cancelled: Invoice Ref #{}'.format(invoice.invoice_reference)
                    )

                else:
                    return Response({'detail': 'Nothing was done'}, status=status.HTTP_400_BAD_REQUEST)

                return Response({'detail': 'Updated'}, status=status.HTTP_200_OK)

        except Exception as e:

            return Response({'detail': 'Not permitted - {}'.format(str(e))}, status=status.HTTP_401_UNAUTHORIZED)

