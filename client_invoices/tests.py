from http import HTTPStatus
from threading import current_thread

from django.test import TestCase, RequestFactory
from django.contrib.auth.models import User
from django.urls import reverse

from client_contracts.tests import create_contract
from client_invoices.models import ClientInvoice
from enterprise.middleware import get_request


class ClientInvoiceTest(TestCase):

    def setUp(self) -> None:

        user = User.objects.create_user(username='Siv', password='password', is_active=True, email='your@email.com')
        self.client.login(username=user.username, password='password')

        self.factory = RequestFactory()

        request = self.factory.get(reverse('enterprise-client-contract-list'))
        request.user = user
        # mimics get_request middleware
        get_request._requests[current_thread()] = request

        self.contract = create_contract(user)

    def test_client_top_up(self):

        superuser = User.objects.create_superuser('admin', 'admin@myproject.com', 'password')
        self.client.login(username=superuser.username, password='password')

        response = self.client.post(reverse('enterprise-client-wallet-transactions-top-up'), data={'amount': 50, 'payment_reference':self.contract.client.payment_reference})

        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_pay_invoice_insufficient_valid(self):

        superuser = User.objects.create_superuser('admin', 'admin@myproject.com', 'password')
        self.client.login(username=superuser.username, password='password')

        self.client.post(reverse('enterprise-client-wallet-transactions-top-up'), data={'amount': 50, 'payment_reference':self.contract.client.payment_reference})

        invoice = ClientInvoice.objects.get(contract=self.contract, status=0)

        invoice_response = self.client.post(reverse('enterprise-client-invoices-make-invoice-update'), data={'invoice_reference':invoice.invoice_reference, 'status':1, 'details':'Paying'})
        print(invoice_response.json())
        self.assertEqual(invoice_response.status_code, HTTPStatus.BAD_REQUEST)

    def test_pay_invoice_sufficient_valid(self):

        superuser = User.objects.create_superuser('admin', 'admin@myproject.com', 'password')
        self.client.login(username=superuser.username, password='password')

        self.client.post(reverse('enterprise-client-wallet-transactions-top-up'), data={'amount': 100, 'payment_reference':self.contract.client.payment_reference})

        invoice = ClientInvoice.objects.get(contract=self.contract, status=0)

        invoice_response = self.client.post(reverse('enterprise-client-invoices-make-invoice-update'), data={'invoice_reference':invoice.invoice_reference, 'status':1, 'details':'Paying'})
        self.assertEqual(invoice_response.status_code, HTTPStatus.OK)



