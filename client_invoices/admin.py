from django.contrib import admin

from client_invoices.models import ClientInvoice

from constance import config


class ClientInvoiceAdmin(admin.ModelAdmin):
    
    def has_add_permission(self, request, obj=None):
        return config.CLIENT_INVOICE_CREATE
    
    # This will help you to disable delete functionaliyt
    def has_delete_permission(self, request, obj=None):
        return config.CLIENT_INVOICE_DELETE

    # This will help you to disable change functionality
    def has_change_permission(self, request, obj=None):
        return config.CLIENT_INVOICE_UPDATE

    class Meta:
        model = ClientInvoice
        
    search_fields = [
        'contract',
        'package',
        'invoice_reference',
        'details',
    ]
    list_display = (
        '__str__',
        'contract',
        'status',
        'package',
        'payable_amount',
        'details',
    )
    readonly_fields=('details', 'origin')

admin.site.register(ClientInvoice, ClientInvoiceAdmin) 