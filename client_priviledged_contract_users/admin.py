from django.contrib import admin, messages
from django.utils.translation import ugettext_lazy as _

from client_priviledged_contract_users.models import ClientPriviledgedContractUsers
from client_utils.permissions import is_privileged_on_other_contract_users


class ClientPriviledgedContractUsersAdmin(admin.ModelAdmin):

    search_fields = [
        'contract',
        'email',
        'user__username',
        'granted_by__username',
        'details'
    ]
    list_display = (
        '__str__',
        'accepted',
        'user',
        'granted_by',
    )
    readonly_fields=('details', )
    exclude = ('granted_by', )

    actions = ("send_message", )

    class Meta:
        model = ClientPriviledgedContractUsers

    def send_message(self, request, queryset):
        """
        Sends user an invitation to accept the privileges granted for contracts
        """
        
        for obj in queryset:

            try:
                obj.send_invivitation()
                self.message_user(
                    request, _("Email sent to {}".format(obj.email))
                )
            except Exception as e:
                self.message_user(
                    request, _("Message could not be processed: {} - {}".format(obj.id, e)),
                    level=messages.ERROR
                )

    def has_add_permission(self, request, obj=None):
        return is_privileged_on_other_contract_users(user=request.user, method=request.method)
    
    def has_view_permission(self, request, obj=None):
        return is_privileged_on_other_contract_users(obj=obj, user=request.user, method=request.method)
    
    def has_change_permission(self, request, obj=None):
        return is_privileged_on_other_contract_users(obj=obj, user=request.user, method=request.method)
    
    def has_delete_permission(self, request, obj=None):
        return is_privileged_on_other_contract_users(obj=obj, user=request.user, method=request.method)

    def get_form(self, request, obj=None, **kwargs):
        
        if not request.user.is_superuser:
            
            if request.user != obj.contract.client.creator or request.user !=  obj.contract.creator:
                self.exclude.append('approve_decisions')
        
        return super(ClientPriviledgedContractUsersAdmin, self).get_form(request, obj, **kwargs)

admin.site.register(ClientPriviledgedContractUsers, ClientPriviledgedContractUsersAdmin)

# dont show form user granted you access or is ownrer