from http import HTTPStatus
from threading import current_thread

from django.test import RequestFactory, TestCase
from django.contrib.auth.models import User
from django.urls import reverse

from client_contracts.tests import create_contract
from enterprise.middleware import get_request


class ClientPriviledgedContractUsersTest(TestCase):

    def setUp(self):
        user = User.objects.create_user(username='Siv', password='password', is_active=True, email='your@email.com')
        self.client.login(username=user.username, password='password')

        self.factory = RequestFactory()

        request = self.factory.get(reverse('enterprise-client-contract-list'))
        request.user = user
        # mimics get_request middleware
        get_request._requests[current_thread()] = request
        
        self.contract = create_contract(user)

    def test_create_priviledged_user_valid(self):

        response = self.client.post(reverse('enterprise-client-priviledged-contract-users-list'), data={'contract': self.contract.id, 'email': 'invited_user@email.com', 'active': True, 'can_invite_other_contract_users': True, 'can_invite_other_contract_users_with_permissions': True, 'can_create_contract': True, 'approve_decisions': True, 'can_update_contract': True, 'can_delete_contract': True, 'can_read_contract_history': True, 'can_read_contract_invoices': True, 'can_read_wallet_transactions': True, 'can_read_other_contract_users': True, 'can_update_other_contract_users': True, 'can_delete_other_contract_users': True})
        
        self.assertEqual(response.status_code, HTTPStatus.CREATED)

    def test_create_priviledged_user_valid_accept_invite(self):

        invited_user = User.objects.create_user(username='Invited', password='password', is_active=True, email='invited_user@email.com')
        
        priviledge = self.client.post(reverse('enterprise-client-priviledged-contract-users-list'), data={'contract': self.contract.id, 'email': invited_user.email, 'active': True, 'can_invite_other_contract_users': True, 'can_invite_other_contract_users_with_permissions': True, 'can_create_contract': True, 'approve_decisions': True, 'can_update_contract': True, 'can_delete_contract': True, 'can_read_contract_history': True, 'can_read_contract_invoices': True, 'can_read_wallet_transactions': True, 'can_read_other_contract_users': True, 'can_update_other_contract_users': True, 'can_delete_other_contract_users': True})
        
        priviledge_result = priviledge.json()

        invite_response = self.client.get(reverse('enterprise-client-priviledged-contract-users-accept-invite', args=[priviledge_result['url']]))
        # self.assertIn("/client-accept-contract-invite/{}/{}/".format(priviledge_result['id'], priviledge_result['url']), invite_response["Location"])

        accept_invite_response = self.client.post(reverse('enterprise-client-accept-contract-invite', args=[priviledge_result['id'], priviledge_result['url']]), data={'username': 'Invited', 'password':'password'})
        # self.assertIn("/client-accepted-contract-invite/{}/{}/".format(priviledge_result['id'], priviledge_result['url']), accept_invite_response["Location"])

        self.assertTrue(invited_user.client_priviledged_user.get(contract= self.contract).accepted)

    def test_priviledged_user_contract_access_invalid(self):

        self.client.post(reverse('enterprise-client-priviledged-contract-users-list'), data={'contract': self.contract.id, 'email': 'invite_user@email.com', 'active': True, 'can_invite_other_contract_users': True, 'can_invite_other_contract_users_with_permissions': True, 'can_create_contract': True, 'approve_decisions': True, 'can_update_contract': True, 'can_delete_contract': True, 'can_read_contract_history': True, 'can_read_contract_invoices': True, 'can_read_wallet_transactions': True, 'can_read_other_contract_users': True, 'can_update_other_contract_users': True, 'can_delete_other_contract_users': True})
        
        invited_user = User.objects.create_user(username='Invited', password='password', is_active=True, email='invited_user@email.com')
        self.client.login(username=invited_user.username, password='password')
        response = self.client.get(reverse('enterprise-client-contract-list'))

        self.assertEqual(response.status_code, HTTPStatus.FORBIDDEN)

    def test_priviledged_user_contract_access_valid(self):

        invited_user = User.objects.create_user(username='Invited', password='password', is_active=True, email='invited_user@email.com')
        
        priviledge = self.client.post(reverse('enterprise-client-priviledged-contract-users-list'), data={'contract': self.contract.id, 'email': invited_user.email, 'active': True, 'can_invite_other_contract_users': True, 'can_invite_other_contract_users_with_permissions': True, 'can_create_contract': True, 'approve_decisions': True, 'can_update_contract': True, 'can_delete_contract': True, 'can_read_contract_history': True, 'can_read_contract_invoices': True, 'can_read_wallet_transactions': True, 'can_read_other_contract_users': True, 'can_update_other_contract_users': True, 'can_delete_other_contract_users': True})
        
        priviledge_result = priviledge.json()

        invite_response = self.client.get(reverse('enterprise-client-priviledged-contract-users-accept-invite', args=[priviledge_result['url']]))
        # self.assertIn("/client-accept-contract-invite/{}/{}/".format(priviledge_result['id'], priviledge_result['url']), invite_response["Location"])

        accept_invite_response = self.client.post(reverse('enterprise-client-accept-contract-invite', args=[priviledge_result['id'], priviledge_result['url']]), data={'username': 'Invited', 'password':'password'})
        # self.assertIn("/client-accepted-contract-invite/{}/{}/".format(priviledge_result['id'], priviledge_result['url']), accept_invite_response["Location"])

        self.client.login(username=invited_user.username, password='password')
        response = self.client.get(reverse('enterprise-client-contract-list'))

        self.assertEqual(response.status_code, HTTPStatus.OK)