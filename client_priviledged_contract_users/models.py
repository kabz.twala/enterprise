from django.db import models, transaction
from django.conf import settings
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import ValidationError

from softdelete.models import SoftDeleteObject

from enterprise.middleware.get_request import get_request
from client_utils.models import CreatedModifiedMixin, PermissionsMixin
from client_utils.permissions import get_privileges_exists_check, is_privileged_on_other_contract_users
from client_utils.views import random_generator
from client_contracts.models import ClientContract

from constance import config
from datetime import timedelta


class ClientPriviledgedContractUsers(CreatedModifiedMixin, PermissionsMixin, SoftDeleteObject):

    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL, related_name='client_priviledged_user')
    contract = models.ForeignKey(ClientContract, null=True, blank=True, on_delete=models.SET_NULL)

    granted_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL, related_name='client_priviledged_granted_by')
    
    email = models.EmailField(blank=False, null=False)

    accepted = models.BooleanField(default=False)
    accepted_date = models.DateTimeField(blank=True, null=True)

    active = models.BooleanField(default=False)

    can_invite_other_contract_users = models.BooleanField(default=False)
    can_invite_other_contract_users_with_permissions = models.BooleanField(default=False)

    approve_decisions = models.BooleanField(default=False)

    can_create_contract = models.BooleanField(default=False)
    can_update_contract = models.BooleanField(default=False)
    can_delete_contract = models.BooleanField(default=False)

    can_read_contract_history = models.BooleanField(default=False)

    can_read_contract_invoices = models.BooleanField(default=False)

    can_read_wallet_transactions = models.BooleanField(default=False)

    can_read_other_contract_users = models.BooleanField(default=False)
    can_update_other_contract_users = models.BooleanField(default=False)
    can_delete_other_contract_users = models.BooleanField(default=False)

    details = models.TextField(null=True, blank=True)

    url = models.TextField(null=True, blank=True)

    clicked = models.PositiveIntegerField(default=0)

    email_response = models.TextField(null=True, blank=True)

    def __init__(self, *args, **kwargs):
        super(ClientPriviledgedContractUsers, self).__init__(*args, **kwargs)
        self._prev_can_read_other_contract_users = self.can_read_other_contract_users

    def __str__(self):
        if self.contract:
            return "{}. {}: {} - {}".format(self.id, self.email, self.contract.client.name, self.contract.contract_reference)
        return str(self.id)

    def save(self, *args, **kwargs):
        request = get_request()
        
        if self.contract is None:
            raise ValidationError("Contract is required")
            
        try:
            with transaction.atomic():

                if self.can_create_contract==True:
                    self.can_invite_other_contract_users = True

                if request:
                    if not self.granted_by:
                        self.granted_by = request.user


                    if request.user != self.contract.client.creator:
                        if request.method == 'POST':
                            privilege = request.user.client_priviledged_user.get(contract_id=self.contract.id, accepted=True, active=True, can_invite_other_contract_users=True)
                            if privilege.can_invite_other_contract_users_with_permissions==False:
                                self.approve_decisions=False

                                self.can_create_contract=False
                                self.can_update_contract=False
                                self.can_delete_contract=False

                                self.can_read_contract_history=False

                                self.can_read_contract_invoices=False

                                self.can_read_wallet_transactions=False

                                self.can_read_other_contract_users=False
                                self.can_update_other_contract_users=False
                                self.can_delete_other_contract_users=False

                        else:
                            if not is_privileged_on_other_contract_users(obj=self, user=request.user, method=request.method):
                                raise ValidationError("User is not permited")
                    
                send_invitation = False

                if not self.url:

                    
                    # exists = ClientPriviledgedContractUsers.objects.filter(contract_id=self.contract.id, email=self.email, accepted=True, active=True).latest('modified')
                    # if exists.exists():
                    #     latest = exists.first()
                    #     exists.excludes(id=latest.id).update(active=False)
                    #     raise ValidationError("Permission with id. {}, already exists.".format(latest.id))
                    if ClientPriviledgedContractUsers.objects.filter(contract_id=self.contract.id, email=self.email, accepted=True, active=True).exists():
                        raise ValidationError("Permission already exists.")


                    while True:
                        url = random_generator(length=20, letters=True, digits=True, punctuation=False, exclude_characters=[])
                        if not type(self).objects.filter(url=url).exists():
                            break

                    self.url = url

                    send_invitation = True


                super(ClientPriviledgedContractUsers, self).save(*args, **kwargs)
                
                if send_invitation:
                    self.send_invivitation()
                    
        except Exception as e:
            raise ValidationError(str(e))

    @classmethod
    def get_queryset(cls, user):

        if not user.is_authenticated:
            return ClientPriviledgedContractUsers.objects.none()

        if user.is_superuser or user.is_staff:
            return ClientPriviledgedContractUsers.objects.all()

        can_view = list( user.client_priviledged_user.filter(accepted=True, active=True, can_read_other_contract_users=True).values_list('id', flat=True) )
        return ClientPriviledgedContractUsers.objects.filter(id__in=can_view, deleted=False).distinct()

    def send_invivitation(self):
        
        from client_utils.tasks import client_contract_priviledged_user_notification

        request = get_request()
        
        client_contract_priviledged_user_notification.apply_async(args=[
            self.id, request.scheme, get_current_site(request).domain, '{} invitation - Contract: #{}'.format(self.contract.client.name, self.contract.contract_reference), config.CLIENT_EMAIL_GREETING, config.CLIENT_EMAIL_FOOTER, config.CLIENT_EMAIL_FROM, self.email, self.url, self.contract.client.name, self.granted_by.email], eta=timezone.localtime(timezone.now()) + timedelta(seconds=30))
        

    @classmethod
    def has_permission(cls, request):
        """ 
        Permissions for Privileged users.
        """

        if request.user.is_superuser or request.user.is_staff:
            return True

        return get_privileges_exists_check(request.user) 

    def has_object_permission(self, request, obj):
        """ 
        Object Permissions for Privileged users.
        """

        if request.user.is_superuser or request.user.is_staff:
            return True

        return is_privileged_on_other_contract_users(obj=obj, user=request.user, method=request.method) 
    
