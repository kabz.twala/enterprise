from django.contrib.auth import get_user_model
from django.contrib.auth.password_validation import validate_password
from django.contrib.auth.validators import ASCIIUsernameValidator
from django.core.validators import validate_email

from rest_framework import serializers
from rest_framework.reverse import reverse as api_reverse

from client_priviledged_contract_users.models import ClientPriviledgedContractUsers


class ClientPriviledgedContractUsersSerializer(serializers.ModelSerializer):

    #client = ClientSerializer(read_only=True)

    # def get_fields(self):
    #     fields = super(ClientPriviledgedContractUsersSerializer, self).get_fields()
    #     # print (self.context['request'].user.client_priviledged_user)
    #     # if getattr(fields, 'can_invite_other_contract_users_with_permissions', False):
    #     #     fields['active'].read_only = True
    #     return fields
     
    client_privilege_uri = serializers.SerializerMethodField()

    class Meta:
        model = ClientPriviledgedContractUsers
        fields = ('id', 'user', 'contract', 'granted_by', 'email', 'accepted', 'accepted_date', 'active', 'can_invite_other_contract_users', 'can_invite_other_contract_users_with_permissions', 'can_create_contract', 'approve_decisions', 'can_update_contract', 'can_delete_contract', 'can_read_contract_history', 'can_read_contract_invoices', 'can_read_wallet_transactions', 'can_read_other_contract_users', 'can_update_other_contract_users', 'can_delete_other_contract_users', 'details', 'url', 'clicked', 'email_response', 'deleted', 'client_privilege_uri')
        read_only_fields = ('id', 'user', 'granted_by', 'accepted', 'accepted_date', 'url', 'clicked', 'email_response', 'details', 'deleted')
    
    def get_client_privilege_uri(self, obj):
        return self.context['request'].build_absolute_uri("/api/enterprise/client-priviledged-contract-users/{id}/".format(id=obj.id))

    def create(self, validated_data):
        
        user = self.context['request'].user
        if user.is_authenticated:
            try:
                validated_data['granted_by'] = user
                return ClientPriviledgedContractUsers.objects.create(**validated_data)
            except Exception as e:
                raise serializers.ValidationError(str(e))
            
        raise serializers.ValidationError('User needs to be signed in')

    def update(self, instance, validated_data):

        user = self.context['request'].user
        if user.is_authenticated:

            for attr, value in validated_data.items():
                setattr(instance, attr, value)
                
            instance.save()
        
            return instance
            
        raise serializers.ValidationError('User needs to be signed in')


class ClientAcceptContractInviteLoginSerializer(serializers.Serializer):

    username = serializers.CharField(
        style={'input_type': 'text', 'placeholder': 'Username', 'autofocus': True},
        required=True
    )
    password = serializers.CharField(
        style={'input_type': 'password', 'placeholder': 'Password'},
        write_only=True, required=True
    )

    class Meta:
        model = get_user_model()
        fields = ('id', 'username', 'password')
        read_only_fields = ('id',)

    def validate(self, attrs):
        
        if len(attrs['username']) == 0:
            raise serializers.ValidationError({"username": "Username is required."})
        elif len(attrs['password']) == 0:
            raise serializers.ValidationError({"password": "Password is required."})

        return attrs


class ClientAcceptContractInviteSerializer(serializers.Serializer):

    username = serializers.CharField(
        style={'input_type': 'text', 'placeholder': 'Username', 'autofocus': True},
        validators=[ASCIIUsernameValidator()]
    )
    password = serializers.CharField(
        style={'input_type': 'password', 'placeholder': 'Password'},
        write_only=True, required=True, validators=[validate_password]
    )
    password2 = serializers.CharField(
        style={'input_type': 'password', 'placeholder': 'Confirm Password'},
        write_only=True, required=True, validators=[validate_password]
    )

    class Meta:
        model = get_user_model()
        fields = ('id', 'username', 'password', 'password2')
        # extra_kwargs = {
        #     'first_name': {'required': True},
        #     'last_name': {'required': True}
        # }
        read_only_fields = ('id',)

        
    def validate(self, attrs):
        
        if len(attrs['username']) == 0:
            raise serializers.ValidationError({"username": "Username is required."})
        elif attrs['password'] != attrs['password2']:
            raise serializers.ValidationError({"password": "Password fields didn't match."})

        return attrs

        
        
