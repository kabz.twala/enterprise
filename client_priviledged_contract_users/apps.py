from django.apps import AppConfig


class ClientPriviledgedContractUsersConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'client_priviledged_contract_users'
