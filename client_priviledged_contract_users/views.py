from django.contrib.auth import get_user_model, login, authenticate
from django.db import transaction
from django.utils import timezone
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect

from rest_framework import status, viewsets, decorators
from rest_framework.reverse import reverse
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from client_priviledged_contract_users.models import ClientPriviledgedContractUsers
from client_priviledged_contract_users.serializers import ClientPriviledgedContractUsersSerializer, ClientAcceptContractInviteSerializer, ClientAcceptContractInviteLoginSerializer
from client_utils.permissions import ModelBasedPermissions
from client_utils.pagination import ClientCustomPagination


class ClientPriviledgedPermission(ModelBasedPermissions):
    model = ClientPriviledgedContractUsers
    message = "You don't have permission to access privileged users"

    
class ClientPriviledgedContractUsersViewSet(viewsets.ModelViewSet):

    model = ClientPriviledgedContractUsers
    serializer_class = ClientPriviledgedContractUsersSerializer
    permission_classes = (IsAuthenticated, ClientPriviledgedPermission)
    pagination_class = ClientCustomPagination
    search_fields = (
        'client',
        'user__username',
        'granted_by__username',
        'details'
    )
    ordering_fields = (
        'id',
        'accepted_date',
        'created',
        'modified'
    )

    def get_queryset(self):
        
        if not self.request.user.is_authenticated:
            raise PermissionDenied()

        queryset = ClientPriviledgedContractUsers.get_queryset(self.request.user)

        if queryset is not None:
            return queryset.order_by('-modified')

        raise PermissionDenied()

    def get_serializer_class(self):
        serializer_class = self.serializer_class

        # if self.request.method == 'PUT':
        #     serializer_class = SerializerWithoutUsernameField
        # if self.action == 'list':
        #     return serializers.ListaGruppi
        # if self.action == 'retrieve':
        #     return serializers.DettaglioGruppi

        return serializer_class

    @decorators.action(detail=False, methods=['get'], permission_classes = (AllowAny, ), url_path="accept-invite/(?P<url>[a-zA-Z0-9]+)")
    def accept_invite(self, request, url=""):
        try:
            privledged = get_object_or_404(ClientPriviledgedContractUsers, url=url)

            try:
                user = get_user_model().objects.get(email=privledged.email)
                privledged.user = user
            except Exception as e:
                pass

            privledged.clicked = privledged.clicked + 1
            if privledged.details:
                privledged.details = privledged.details + '\n\n{} - URL was clicked'.format( timezone.localtime(timezone.now()) )
            else:
                privledged.details = '{} - URL was clicked'.format( timezone.localtime(timezone.now()) )
            privledged.save()

            if privledged.user and privledged.accepted:
                if privledged.user.is_authenticated:
                    return HttpResponseRedirect(reverse( 'enterprise-client-accepted-contract-invite',  kwargs={'pk': privledged.id, 'url': url}, request=request))

            # auto login if user has already accepted
            # if privledged.user and privledged.accepted:
            #     if not privledged.user.is_authenticated:
            #         login(request=request, user=privledged.user)
                
            #     return HttpResponseRedirect(reverse( 'enterprise-client-accepted-contract-invite',  kwargs={'pk': privledged.id, 'url': url}, request=request))

            return HttpResponseRedirect(reverse('enterprise-client-accept-contract-invite', kwargs={'pk': privledged.id, 'url': url}, request=request))
        except Exception as e:
            print(e)
            return Response({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)


class ClientAcceptContractInviteViewSet(APIView):

    renderer_classes = [TemplateHTMLRenderer]
    template_name='accept_invite.html'
    permission_classes = (AllowAny, )

    def get(self, request, **kwargs):
        url = kwargs.get('url', None)
        pk = kwargs.get('pk', None)

        try:
            privledged = get_object_or_404(ClientPriviledgedContractUsers, pk=pk, url=url)

            if privledged.user:
                if privledged.accepted and privledged.user.is_authenticated:
                    return HttpResponseRedirect(reverse( 'enterprise-client-accepted-contract-invite',  kwargs={'pk': privledged.id, 'url': url}, request=request))
                # auto login if user is already logged in
                # if privledged.user.is_authenticated:
                #     return HttpResponseRedirect(reverse( 'enterprise-client-accepted-contract-invite',  kwargs={'pk': privledged.id, 'url': url}, request=request))
                serializer = ClientAcceptContractInviteLoginSerializer(privledged.user)
            else:  
                serializer = ClientAcceptContractInviteSerializer(privledged.user)

            return Response({'serializer': serializer, 'pk': pk, 'url':url})
        except Exception as e:
            return Response({'detail': str(e),'err': True,  'pk': pk, 'url':url}, status=status.HTTP_400_BAD_REQUEST)
    
    def post(self, request, pk, **kwargs):

        url = kwargs.get('url', None)

        try:
            privledged = get_object_or_404(ClientPriviledgedContractUsers, url=url, pk=pk)

            if privledged.user:
                # auto login if user is already logged in
                # if privledged.user.is_authenticated:
                #     return HttpResponseRedirect(reverse( 'enterprise-client-accepted-contract-invite',  kwargs={'pk': privledged.id, 'url': url}, request=request))
                serializer = ClientAcceptContractInviteLoginSerializer(privledged.user, data=request.data)
            else:  
                serializer = ClientAcceptContractInviteSerializer(privledged.user, data=request.data)

            if not serializer.is_valid():
                return Response({'serializer': serializer, 'pk': pk, 'url':url}, status=status.HTTP_400_BAD_REQUEST)
            else:
                
                try:
                    with transaction.atomic():
                        
                        if privledged.user:
                            
                            user = authenticate(request, username=privledged.user.username, password=str(serializer.validated_data['password']))

                            if user is None:
                                return Response({'detail': "Incorrect login details", 'err': True, 'pk': pk, 'url':url, 'serializer': serializer}, status=status.HTTP_401_UNAUTHORIZED)
                        else:
                            user = get_user_model().objects.create(
                                email=privledged.email,
                                username=serializer.data.get('username'),
                                is_active=True
                            )

                            user.set_password(serializer.validated_data['password'])
                            user.save()
                            

                            privledged.contract.client.send_email(
                                origin='ClientAcceptContractInviteViewSet.post', 
                                details='Invite accepted from company, {}. {} - #{}'.format(privledged.contract.id, privledged.contract.client.name, privledged.contract.contract_reference),
                                recipients=[user.id],
                                subject='Invite accepted - #{}'.format(privledged.contract.contract_reference), 
                                message_body='You have successfully accepted your invite granted by {} for the contract: {}. {} - #{}'.format(privledged.granted_by.username, privledged.contract.client.id, privledged.contract.client.name, privledged.contract.contract_reference)
                            )

                        privledged.user = user
                        privledged.accepted = True
                        privledged.accepted_date = timezone.now()
                        privledged.details = privledged.details + '\n\n{} - Invite accepted'.format( timezone.localtime(timezone.now()) )
                        privledged.save()

                        login(request, user)
                        
                        return HttpResponseRedirect(reverse('enterprise-client-accepted-contract-invite', kwargs={'pk': privledged.id, 'url': privledged.url}, request=request))
                
                except Exception as e:
                    return Response({'detail': str(e), 'err': True, 'pk': pk, 'url':url, 'serializer': serializer}, status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            return Response({'detail': str(e), 'err': True,  'pk': pk, 'url':url})


class ClientAcceptedContractInviteViewSet(APIView):

    renderer_classes = [JSONRenderer, TemplateHTMLRenderer]
    template_name='accepted_invite.html'
    permission_classes = (IsAuthenticated, )
        
    def get(self, request, **kwargs):

        try:

            pk = kwargs.get('pk', None)
            url = kwargs.get('url', None)
            message = kwargs.get('message', "Invite already accepted")
            get_object_or_404(ClientPriviledgedContractUsers, pk=pk, url=url, user=request.user)
            
            return Response({'message': message, 'err': False}, status=status.HTTP_200_OK)

        except Exception as e:
            return Response({'detail': str(e), 'err': True}, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request):

        return PermissionDenied()


