from django import forms

class ClientPackagesForm(forms.ModelForm):
    
    def clean(self):
        category = self.cleaned_data['category']
        included_packages = self.cleaned_data['included_packages']
        for included_package in included_packages:
            if included_package.category != category:
                raise forms.ValidationError({'included_packages': 'Included packages should belong to same category as the selected package'})