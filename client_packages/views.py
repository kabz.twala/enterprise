from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated

from client_packages.models import ClientPackage
from client_packages.serializers import ClientPackageSerializer
from client_utils.permissions import IsStaffOrSuperuser
from client_utils.pagination import ClientCustomPagination


class ClientPackageViewSet(viewsets.ModelViewSet):

    model = ClientPackage
    serializer_class = ClientPackageSerializer
    permission_classes = (IsAuthenticated, IsStaffOrSuperuser)
    pagination_class = ClientCustomPagination
    search_fields = (
        'name',
        'price',
        'category',
    )
    ordering_fields = (
        'id',
        'name',
        'price',
        'category',
        'duration_in_days',
        'created',
        'modified'
    )

    def get_queryset(self):
        
        if not self.request.user.is_authenticated:
            raise PermissionDenied()

        queryset = ClientPackage.get_queryset()

        if queryset is not None:
            return queryset.order_by('-modified')

        raise PermissionDenied()
    

    @action(methods=['post'], detail=True)
    def swap(self, request, pk):
        '''
        create different serializer urls for api calls to move_up, move_down, swap, etc for ordering including delete
        '''


