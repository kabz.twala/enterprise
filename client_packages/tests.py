from http import HTTPStatus

from django.test import TestCase
from django.contrib.auth.models import User
from django.urls import reverse

from client_package_categories.models import ClientPackageCategory
from client_packages.models import ClientPackage, ClientPackageFeature


class ClientPackageTest(TestCase):

    def setUp(self):
        self.category = ClientPackageCategory.objects.create(title='Payment Gateway')

        self.package_1 = ClientPackage.objects.create(name = "Cardless payments", active = True, category = self.category, duration_in_days = 2, use_feature_total = False, price = 20, tax_or_vat_price = 0, description = "")
        self.package_2 = ClientPackage.objects.create(name = "Crypto payments", active = True, category = self.category, duration_in_days = 3, use_feature_total = False, price = 30, tax_or_vat_price = 0, description = "")
        self.package_3 = ClientPackage.objects.create(name = "Mobile payments", active = True, category = self.category, duration_in_days = 5, use_feature_total = False, price = 50, tax_or_vat_price = 0, description = "")
    
    def test_create_package_valid(self):
        
        superuser = User.objects.create_superuser('admin', 'admin@myproject.com', 'password')
        self.client.login(username=superuser.username, password='password')

        response = self.client.post(reverse('enterprise-client-packages-list'), data={"name": "Crypto payments", "active": True, "category": self.category.id, "duration_in_days": 10, "use_feature_total": False, "price": 100, "tax_or_vat_price": 0, "description": ""})

        self.assertEqual(response.status_code, HTTPStatus.CREATED)

    def test_included_package_valid(self):

        package = ClientPackage.objects.create(name = "Card payments", active = True, category = self.category, duration_in_days = 10, use_feature_total = False, price = 100, tax_or_vat_price = 0, description = "")
        package.included_packages.set([self.package_1])

        self.assertEqual(package.included_packages.count(), 1)

    # sum of prices for package (100), package_1 (20), package_2 (30) and package_3 (50) = 200
    def test_get_price_of_included_packages(self):

        package = ClientPackage.objects.create(name = "Card payments", active = True, category = self.category, duration_in_days = 10, use_feature_total = False, price = 100, tax_or_vat_price = 0, description = "")
        package.included_packages.set([self.package_1, self.package_2, self.package_3])

        self.assertEqual(package.get_price(), 200)

    # sum of prices for package features (40), package_1 (20), package_2 (30) and package_3 (50) = 140
    def test_get_price_of_included_packages_and_features(self):

        package = ClientPackage.objects.create(name = "Card payments", active = True, category = self.category, duration_in_days = 10, use_feature_total = True, price = 100, tax_or_vat_price = 0, description = "")
        package.included_packages.set([self.package_1, self.package_2, self.package_3])

        ClientPackageFeature.objects.create(name='Tap', package=package, price=40)

        self.assertEqual(package.get_price(), 140)


