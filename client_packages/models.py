from collections import namedtuple
from django.db import models
from django.conf import settings
from django.db.models import Sum
from django.contrib.sites.models import Site

from client_utils.models import CreatedModifiedMixin, PermissionsMixin
from client_utils.views import get_field_choices
from client_package_categories.models import ClientPackageCategory

from versatileimagefield.fields import VersatileImageField, PPOIField
from ordered_model.models import OrderedModel

def image_icon_upload_to(instance, filename):
    return 'client/img/packages/image_icon/{0}/{1}'.format(instance.name, filename)
            

'''
1. Create packages first
2. Client can now be created using a default package 
https://github.com/django-ordered-model/django-ordered-model
'''
class ClientPackage(CreatedModifiedMixin, PermissionsMixin, OrderedModel):
    '''
    strictly used by admin or site owners
    '''

    name = models.CharField(max_length=255, null=False, blank=False)

    active = models.BooleanField(default=True)

    creator = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL, related_name="package_creator")

    category = models.ForeignKey(ClientPackageCategory, null=False, blank=False, on_delete=models.CASCADE)

    site = models.OneToOneField(Site, null=True, blank=True, on_delete=models.SET_NULL)

    # https://adamj.eu/tech/2021/02/26/django-check-constraints-prevent-self-following/
    included_packages = models.ManyToManyField('self', blank=True, symmetrical=False, through='ClientRelatedPackage')

    image_icon = VersatileImageField(
        'Image',
        upload_to=image_icon_upload_to,
        ppoi_field='image_ppoi',
        null=True, blank=True
    )
    image_ppoi = PPOIField()

    description = models.TextField(null=True, blank=True)

    # duration_number = models.PositiveIntegerField(default=0, null=False, blank=False)
    # duration_choices = (
    #     'days',
    #     'months',
    #     'years'
    # )
    # DURATION_TYPE = namedtuple('DURATION_TYPE', duration_choices)(*range(0, len(duration_choices)))
    # duration = models.PositiveIntegerField(default=0, choices=get_field_choices(DURATION_TYPE))

    duration_in_days = models.PositiveIntegerField(default=1)

    use_feature_total = models.BooleanField(default=False) 
    price = models.DecimalField(default=0.00, decimal_places=2, max_digits=8)
    tax_or_vat_price = models.DecimalField(default=0.00, decimal_places=2, max_digits=8)

    ribbon_text = models.CharField(max_length=20, null=True, blank=True, default="")
    ribbon_text_color = models.CharField(max_length=20, null=True, blank=True, default="")
    ribbon_background_color = models.CharField(max_length=20, null=True, blank=True, default="")

    order_with_respect_to = 'category'

    def __str__(self):
        return "{}. {} - {}".format(self.id, self.name, self.category.title)


    @classmethod
    def get_queryset(cls):
        return ClientPackage.objects.exclude(included_packages=models.F('pk'))

    class Meta:
        ordering = ('category', 'order')
    
    
    def rate_per_day(self):
        '''
        Returns rate per day with 2 decimal places
        '''
        try:
            return round(self.get_price() / self.duration_in_days, 2)
        except Exception as e:
            print(e)
            return 0


    def total_price(self):
        try:
            if self.use_feature_total:
                return round( self.clientpackagefeature.aggregate(Sum('price'))['price__sum'], 2 )
            return self.price
        except Exception as e:
            print(e)
            return self.price

    def get_price(self):
        '''
        Returns total price charged with 2 decimal places
        '''
        price = self.total_price()
        for package in self.included_packages.all():
            # recursive
            price = price + package.get_price()
            # print(str(package.id) + " " +str(price))
        return price
    

    def features(self):
        try:
            return list(self.clientpackagefeature.all().values('name', 'description', 'price'))
        except Exception as e:
            print(e)
            return []

    def get_features(self):
        '''
        Returns all features including children features
        '''
        feature_list = self.features()
        for package in self.included_packages.all():
            # recursive
            feature_list.extend(package.get_features())
        return list(feature_list)


class ClientRelatedPackage(CreatedModifiedMixin):

    package_from = models.ForeignKey(ClientPackage, on_delete=models.CASCADE, related_name="+")
    package_to = models.ForeignKey(ClientPackage, on_delete=models.CASCADE, related_name="+")

    class Meta:
        constraints = [
            models.UniqueConstraint(
                name="%(app_label)s_%(class)s_unique_relationships",
                fields=["package_from", "package_to"],
            ),
            models.CheckConstraint(
                name="%(app_label)s_%(class)s_prevent_self_follow",
                check=~models.Q(package_from=models.F("package_to")),
            )
        ]
        

class ClientPackageFeature(CreatedModifiedMixin, PermissionsMixin, OrderedModel):
    '''
    strictly used by admin or site owners
    '''

    name = models.CharField(max_length=255, null=False, blank=False)

    package = models.ForeignKey(ClientPackage, null=False, blank=False, on_delete=models.CASCADE, related_name='clientpackagefeature')

    price = models.DecimalField(default=0.00, decimal_places=2, max_digits=8)

    order_with_respect_to = 'package'

    description = models.TextField(null=True, blank=True)
    
    def __str__(self):
        return "{}. {}".format(self.id, self.name)

    @classmethod
    def get_queryset(cls):
        return ClientPackageFeature.objects.all()

    class Meta:
        ordering = ('package', 'order')
