from django.db import transaction

from rest_framework import serializers
from rest_framework.reverse import reverse as api_reverse
from versatileimagefield.serializers import VersatileImageFieldSerializer

from client_packages.models import ClientPackage, ClientPackageFeature


class ClientPackageFeatureSerializer(serializers.ModelSerializer):

    class Meta:
        model = ClientPackageFeature
        fields = ('id', 'name', 'price', 'description', 'order')
        read_only_fields = ('id', 'order')


class PackageFilteredSlugRelatedField(serializers.SlugRelatedField):

    def get_queryset(self):
        request = self.context.get('request', None)
        queryset = super(PackageFilteredSlugRelatedField, self).get_queryset()

        if not request or not queryset:
            return None

        try:
            package = ClientPackage.objects.get(id=request.resolver_match.kwargs.get('pk'))
            return queryset.filter(category=package.category).exclude(pk=package.id)
        except:
            return queryset.all()


class ClientPackageSerializer(serializers.ModelSerializer):

    #lyrics = serializers.CharField(trim_whitespace=False)

    image_icon = VersatileImageFieldSerializer(
        sizes='product_headshot',
        required=False,
        allow_null=True
    )

    '''
    RecursiveField
    https://stackoverflow.com/questions/13376894/django-rest-framework-nested-self-referential-objects#answer-27236783
    https://github.com/heywbj/django-rest-framework-recursive
    '''
    '''
    read_only=False, write_only=False,
    required=None, default=empty, initial=empty, source=None,
    label=None, help_text=None, style=None,
    error_messages=None, validators=None, allow_null=False
    '''
    client_package_uri = serializers.SerializerMethodField(read_only=True)

    features = serializers.SerializerMethodField()

    active = serializers.BooleanField(initial=True)

    included_packages = PackageFilteredSlugRelatedField(many=True, slug_field="name", queryset=ClientPackage.objects)

    class Meta:
        model = ClientPackage
        fields = ('id', 'name', 'active', 'creator', 'category', 'site', 'included_packages', 'duration_in_days', 'use_feature_total', 'get_price', 'rate_per_day', 'price', 'tax_or_vat_price', 'description', 'features', 'image_icon', 'ribbon_text', 'ribbon_text_color', 'ribbon_background_color', 'order', 'client_package_uri')
        read_only_fields = ('id', 'creator', 'order')
    
    def get_features(self, obj):
        return [ClientPackageFeatureSerializer(p).data for p in obj.clientpackagefeature.all()]

    def get_client_package_uri(self, obj):
        return self.context['request'].build_absolute_uri("/api/enterprise/client-packages/{id}/".format(id=obj.id))
        # request = self.context.get('request')
        # return api_reverse('enterprise-client-packages-detail', kwargs={'id': obj.id}, request=request) e.g. -list, -detail, python manage.py show_urls

    def validate_name(self, value):

        if len(value) == 0:
            raise serializers.ValidationError("Name is required")
        else:
            try:
                ClientPackage.objects.get(name=value)
                raise serializers.ValidationError("Name is already in use")
            except:
                pass

        return value
                
    def create(self, validated_data):
        
        user = self.context['request'].user
        if user.is_superuser or user.is_staff:
        
            with transaction.atomic():
                try:
                    feature_data = self.context['request'].data.get('features', [])
                    
                    if 'included_packages' in validated_data:
                        included_packages = validated_data.pop('included_packages')
                    else:
                        included_packages = []
                    
                    instance = ClientPackage.objects.create(**validated_data)

                    for included_package in included_packages:
                        if included_package.category != instance.category:
                            raise serializers.ValidationError("Included packages should belong to same category as the selected package")

                        instance.included_packages.add(included_package)

                    instance.creator = self.context['request'].user
                    
                    for feature in feature_data:
                        try:
                            feature['package'] = instance
                            if 'order' in feature:
                                del feature['order']
                            ClientPackageFeature.objects.create(**feature)
                        except:
                            pass

                    return instance

                except Exception as e:
                    raise serializers.ValidationError(str(e))
        
        raise serializers.ValidationError('User is not permitted')

    def update(self, instance, validated_data):

        user = self.context['request'].user
        if user.is_superuser or user.is_staff:

            with transaction.atomic():

                try:
                    feature_data = self.context['request'].data.get('features', [])

                    if 'included_packages' in validated_data:
                        included_packages = validated_data.pop('included_packages')
                    else:
                        included_packages = []

                    for attr, value in validated_data.items():
                        setattr(instance, attr, value)

                    instance.included_packages.clear()

                    for included_package in included_packages:
                        if included_package.category != instance.category:
                            raise serializers.ValidationError("Included packages should belong to same category as the selected package")

                        instance.included_packages.add(included_package)

                    for feature in feature_data:
                        try:
                            ClientPackageFeature.objects.update_or_create(
                                id=feature.get('id', None),
                                # update these fields, or create a new object with these values
                                defaults={
                                    'name': feature.get('name', None), 'price': feature.get('price', None), 'description': feature.get('description', None), 'package': instance
                                }
                            )

                        except Exception as e:
                            raise serializers.ValidationError("Feature Error: " + str(e))

                    instance.save()
        
                    return instance

                except Exception as e:
                    raise serializers.ValidationError(str(e))

        raise serializers.ValidationError('User is not permitted')

