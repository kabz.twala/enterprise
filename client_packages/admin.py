from django.contrib import admin, messages
from django.db import transaction
from django.http import HttpResponseRedirect

from client_packages.models import ClientPackage, ClientPackageFeature
from client_packages.forms import ClientPackagesForm

from ordered_model.admin import OrderedTabularInline, OrderedInlineModelAdminMixin, OrderedModelAdmin
from rest_framework.exceptions import PermissionDenied


class ClientPackageFeatureInline(OrderedTabularInline):
    model = ClientPackageFeature
    fields = [
        'name',
        'price',
        'order',
        'move_up_down_links',
        'description',
    ]
    readonly_fields = ('order', 'move_up_down_links',)
    ordering = ('order',)
    extra = 1


class ClientPackageAdmin(OrderedInlineModelAdminMixin, OrderedModelAdmin):
    '''
    https://stackoverflow.com/questions/40782792/disallow-django-self-referential-foreign-key-to-point-to-self-object#answer-40784437
    '''
    
    form = ClientPackagesForm
    search_fields = [
        'name',
        'category__title',
        'description',
        'category',
        'site',
    ]
    list_display = (
        '__str__',
        'active',
        'price',
        'order',
        'category',
        'move_up_down_links',
        'site',
    )
    # list_display_links = ('__str__',)

    inlines = [
        ClientPackageFeatureInline,
    ]

    ordering = ('category', 'order',)

    class Meta:
        model = ClientPackage

    # if package_upgrade is not excluded
    def get_form(self, request, obj=None, **kwargs):
        self.object = None
        if obj:
            self.object = obj
        return super().get_form(request, obj, **kwargs)

    def formfield_for_manytomany(self, db_field, request, obj=None, **kwargs):
        if self.object and db_field.name == "included_packages":
            kwargs['queryset'] = ClientPackage.objects.filter(category=self.object.category).exclude(pk=self.object.id)
        return super(ClientPackageAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)
    
    def save_model(self, request, obj, form, change):
        if not request.user.is_superuser or not request.user.is_staff:
            return PermissionDenied()

        package = super().save_model(request, obj, form, change)

        for included_package in ClientPackage.objects.filter(pk__in=form.data.getlist('included_packages', [])):
            obj.included_packages.add(included_package)

        return package
        

admin.site.register(ClientPackage, ClientPackageAdmin)

