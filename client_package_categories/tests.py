from http import HTTPStatus

from django.test import TestCase
from django.contrib.auth.models import User
from django.urls import reverse

from client_package_categories.models import ClientPackageCategory 


class ClientPackageCategoryTest(TestCase):

    def setUp(self):
        self.superuser = User.objects.create_superuser('admin', 'admin@myproject.com', 'password')
        self.category = ClientPackageCategory.objects.create(title='Payment Gateway')

    # should pass | superuser is logged in
    def test_create_category_valid(self, title='Escrow'):
        
        self.client.login(username=self.superuser.username, password='password')

        response = self.client.post(reverse('enterprise-client-package-categories-list'), data={'title': title})
        
        self.assertEqual(response.status_code, HTTPStatus.CREATED)

    # should fail | superuser is not logged in
    def test_create_category_invalid(self, title='Escrow'):
        
        response = self.client.post(reverse('enterprise-client-package-categories-list'), data={'title': title})
        
        self.assertEqual(response.status_code, HTTPStatus.UNAUTHORIZED)

    # should pass | self.category has children
    def test_category_has_children_valid(self, title='Escrow'):
        
        self.client.login(username=self.superuser.username, password='password')

        self.client.post(reverse('enterprise-client-package-categories-list'), data={'title': title, 'parent_category': self.category.id})
        
        self.assertTrue(self.category.has_children())

