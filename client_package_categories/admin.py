from django.contrib import admin

from client_package_categories.models import ClientPackageCategory

from rest_framework.exceptions import PermissionDenied


class ClientPackageCategoryAdmin(admin.ModelAdmin):
    '''
    https://stackoverflow.com/questions/40782792/disallow-django-self-referential-foreign-key-to-point-to-self-object#answer-40784437
    '''
        
    search_fields = [
        'category_type',
        'title',
        'description',
    ]
    list_display = (
        '__str__',
        'category_type',
        'description',
    )

    class Meta:
        model = ClientPackageCategory
        
    exclude = ('parent_category', )

    # if parent_category is not excluded
    # def change_view(self, request, object_id, form_url='', extra_context=None):
    #     self.object_id = object_id
    #     return super(ClientPackageCategoryAdmin, self).change_view(
    #         request, object_id, form_url, extra_context=extra_context,
    #     )

    # def formfield_for_foreignkey(self, db_field, request, **kwargs):
    #     if db_field.name == "parent_category":
    #         kwargs['queryset'] = ClientPackageCategory.objects.exclude(pk=self.object_id)
    #     return super(ClientPackageCategoryAdmin, self).formfield_for_foreignkey(
    #         db_field, request, **kwargs)

    def save_model(self, request, obj, form, change):
        if request.user.is_superuser or request.user.is_staff:
            return PermissionDenied()
        super().save_model(request, obj, form, change)

admin.site.register(ClientPackageCategory, ClientPackageCategoryAdmin)

