from django.db import transaction

from rest_framework import serializers

from client_packages.models import ClientPackageCategory


class CategorySerializer(serializers.ModelSerializer):
    has_children = serializers.BooleanField(read_only=True)
    class Meta:
        model = ClientPackageCategory
        fields = ('id', 'title', 'depth', 'has_children')


class ClientPackageCategorySerializer(serializers.ModelSerializer):

    children = CategorySerializer(many=True, read_only=True, source='get_child_categories')
    has_children = serializers.BooleanField(read_only=True)

    class Meta:
        model = ClientPackageCategory
        fields = ('id', 'category_type', 'title', 'depth', 'has_children', 'parent_category', 'children')
        read_only_fields = ('id', 'category_type', 'depth')
        extra_kwargs = {"title": {"trim_whitespace": True}}
    
    def validate_title(self, value):

        if len(value) == 0:
            raise serializers.ValidationError("Category title is required")

        return value
                
    def create(self, validated_data):
        
        user = self.context['request'].user
        if user.is_superuser:
            
            try:
                with transaction.atomic():
                    return ClientPackageCategory.objects.create(**validated_data)
            except Exception as e:
                raise serializers.ValidationError(str(e))
        
        raise serializers.ValidationError('User is not permitted')

    def update(self, instance, validated_data):

        user = self.context['request'].user
        if user.is_superuser:
            
            try:
                with transaction.atomic():

                    for attr, value in validated_data.items():
                        setattr(instance, attr, value)
                    
                    instance.save()
                    
                    return instance

            except Exception as e:
                raise serializers.ValidationError(str(e))
        
        raise serializers.ValidationError('User is not permitted')