from rest_framework import viewsets
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated

from client_package_categories.models import ClientPackageCategory
from client_package_categories.serializers import ClientPackageCategorySerializer
from client_utils.permissions import IsStaffOrSuperuser
from client_utils.pagination import ClientCustomPagination


class ClientPackageCategoryViewSet(viewsets.ModelViewSet):

    model = ClientPackageCategory
    serializer_class = ClientPackageCategorySerializer
    permission_classes = (IsAuthenticated, IsStaffOrSuperuser)
    pagination_class = ClientCustomPagination
    search_fields = (
        'category_type',
        'title',
        'description',
        'parent_category',
    )
    ordering_fields = (
        'id',
        'category_type',
        'created',
        'modified'
    )

    def get_queryset(self):
        
        if not self.request.user.is_authenticated:
            raise PermissionDenied()

        queryset = ClientPackageCategory.get_queryset(self.request.user)

        if queryset is not None:
            return queryset.order_by('-modified')

        raise PermissionDenied()


