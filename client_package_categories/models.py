from django.db import models
from django.core.exceptions import ValidationError

from client_utils.models import CreatedModifiedMixin, PermissionsMixin
from client_utils.views import replace_puntcuation

from rest_framework.exceptions import PermissionDenied


class ClientPackageCategory(CreatedModifiedMixin, PermissionsMixin):

    category_type = models.CharField(max_length=255, null=False, blank=False, unique=True)
    title = models.CharField(max_length=255, null=False, blank=False, unique=True)
    description = models.TextField()
    parent_category = models.ForeignKey('self', blank=True, null=True, on_delete=models.SET_NULL)
    depth = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        verbose_name = 'ClientPackageCategory'
        verbose_name_plural = 'ClientPackageCategories'

    def __str__(self):
        return "{}. {}".format(self.id, self.title)

    def save(self, *args, **kwargs):

        if self.parent_category == self:
            raise ValidationError("A category can't be its own parent")
            
        if self.category_type:
            self.category_type = replace_puntcuation(word=self.category_type, replace_with='_').lower().strip()
        else:
            self.category_type = replace_puntcuation(word=self.title, replace_with='_').lower().strip()

        if self.parent_category:
            self.depth = self.parent_category.depth + 1
        else:
            self.depth = 0

        super(ClientPackageCategory, self).save(*args, **kwargs)


    def get_child_categories(self):

        open_list = list(ClientPackageCategory.objects.filter(parent_category=self))
        closed_list = []

        while len(open_list) > 0:
            category = open_list.pop()
            closed_list.append(category)
            children = ClientPackageCategory.objects.filter(parent_category=category)
            child_categories = [x for x in children if x not in closed_list and x not in open_list]
            open_list += child_categories

        return closed_list

    def has_children(self):

        children = list(ClientPackageCategory.objects.filter(parent_category=self))
        return True if children else False

    @classmethod
    def get_queryset(cls, user):

        if user.is_superuser or user.is_staff:
            return ClientPackageCategory.objects.exclude(parent_category=models.F('pk'))

        return PermissionDenied()