from django.db import models
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site

from softdelete.models import SoftDeleteObject

from client_utils.models import CreatedModifiedMixin, PermissionsMixin
from enterprise.middleware.get_request import get_request
from client_utils.views import random_generator, set_config_value
from client_utils.tasks import send_client_emails


class Client(CreatedModifiedMixin, PermissionsMixin, SoftDeleteObject):
    """
    Django model tracking all Enterprise clients

    :author: Kabelo Twala
    """

    active = models.BooleanField(default=True)

    name = models.CharField(max_length=50, unique=True)
    
    payment_reference = models.CharField(max_length=10, null=True, blank=True, unique=True)

    # amount loaded into their wallet
    wallet = models.DecimalField(default=0.00, decimal_places=2, max_digits=8)

    creator = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL, related_name="client_creator")

    def __str__(self):
        return "{}. {}".format(self.id, self.name)

    def save(self, *args, **kwargs):

        super(Client, self).save(*args, **kwargs)

        if not self.payment_reference:

            request = get_request()
            
            while True:
                payment_reference = random_generator(length=7, letters=True, digits=True, punctuation=False)
                if not type(self).objects.filter(payment_reference=payment_reference).exists():
                    break
                
            self.payment_reference = payment_reference

            self.send_email(
                origin='ClientSerializer.create', 
                details='New client, {}. {}, was created'.format(self.id, self.name),
                recipients=[self.creator.id],
                status=0, 
                subject='New client profile created - {}'.format(self.name), 
                message_body='Welcome aboard. Please use your payment reference when making payments: {}'.format(payment_reference), 
                view_in_browser_url=request.build_absolute_uri('/api/enterprise/clients/{}/'.format(self.id))
            )
            self.save()
            
    
    @classmethod
    def get_queryset(cls, user):
        
        if not user.is_authenticated:
            return Client.objects.none()

        if user.is_superuser or user.is_staff:
            return Client.objects.all()

        return Client.objects.filter(creator=user)

    def send_email(self, packages=[], amount=0, tax_or_vat_price=0, origin='Clients.send_email', recipients=[], invoice=False, invoice_reference='', invoice_date='', details='', status=0, subject='', message_body='', view_in_browser_url=''):

        request = get_request()
        user_id = None
        current_site_id = None

        if request:
            set_config_value('SITE_URL', request.build_absolute_uri('/'))
            current_site_id = get_current_site(request).id
            user_id = request.user.id
        
        send_client_emails.delay(user_id, current_site_id, client_id=self.id, packages=packages, amount=amount, tax_or_vat_price=tax_or_vat_price, origin=origin, recipients=recipients, invoice=invoice, invoice_reference=invoice_reference, invoice_date=invoice_date, details=details, status=status, subject=subject, message_body=message_body, view_in_browser_url=view_in_browser_url)
        