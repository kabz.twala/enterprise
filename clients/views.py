from django.contrib.auth import get_user_model, login, authenticate
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect

from rest_framework import status, viewsets
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from clients.models import Client
from clients.serializers import ClientSerializer
from client_utils.permissions import IsStaffOrSuperuser
from client_utils.pagination import ClientCustomPagination
from client_priviledged_contract_users.serializers import ClientAcceptContractInviteLoginSerializer


class ClientViewSet(viewsets.ModelViewSet):

    model = Client
    serializer_class = ClientSerializer
    permission_classes = (IsAuthenticated, )
    pagination_class = ClientCustomPagination
    search_fields = (
        'name',
        'payment_reference'
    )
    ordering_fields = (
        'id',
        'name',
        'created',
        'modified'
    )

    def get_queryset(self):
        
        if not self.request.user.is_authenticated:
            raise PermissionDenied()

        queryset = Client.get_queryset(self.request.user)

        if queryset is not None:
            return queryset.order_by('-modified')

        raise PermissionDenied()

class ClientLoginViewSet(APIView):

    renderer_classes = [TemplateHTMLRenderer]
    template_name='login.html'
    permission_classes = (AllowAny, )

    def get(self, request, **kwargs):

        if request.user.is_authenticated:
            return Response({'detail': 'User already logged in','err': True}, status=status.HTTP_200_OK)

        else:
            serializer = ClientAcceptContractInviteLoginSerializer(request.user)
            return Response({'serializer': serializer})
    
    def post(self, request, **kwargs):

        try:

            serializer = ClientAcceptContractInviteLoginSerializer(request.user, data=request.data)

            if request.user.is_authenticated:
                return Response({'detail': 'User already logged in','err': True}, status=status.HTTP_200_OK)
            
            elif not serializer.is_valid():
                return Response({'serializer': serializer}, status=status.HTTP_400_BAD_REQUEST)
            
            else:
                
                try:
                            
                    user = authenticate(request, username=str(serializer.validated_data['username']), password=str(serializer.validated_data['password']))

                    if user is None:
                        return Response({'detail': "Incorrect login details", 'err': True, 'serializer': serializer}, status=status.HTTP_401_UNAUTHORIZED)
                        
                    login(request, user)
                    
                    return Response({'detail': 'User is now logged in','err': True}, status=status.HTTP_200_OK)
                
                except Exception as e:
                    return Response({'detail': str(e), 'err': True, 'serializer': serializer}, status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            return Response({'detail': str(e), 'err': True}, status=status.HTTP_400_BAD_REQUEST)

