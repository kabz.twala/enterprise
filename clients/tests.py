from http import HTTPStatus
from django.test import TestCase

from django.contrib.auth.models import User
from django.urls import reverse


class ClientTest(TestCase):

    def test_create_client_valid(self):
        user = User.objects.create_user(username='Siv', password='password', is_active=True, email='your@email.com')
        self.client.login(username=user.username, password='password')

        response = self.client.post(reverse('enterprise-clients-list'), data={'name': 'Client 1'})

        self.assertEqual(response.status_code, HTTPStatus.CREATED)
