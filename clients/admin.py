from django.contrib import admin

from clients.models import Client

from constance import config


class ClientAdmin(admin.ModelAdmin):

    def has_add_permission(self, request, obj=None):
        return config.CLIENT_CREATE
    
    # This will help you to disable delete functionaliyt
    def has_delete_permission(self, request, obj=None):
        return config.CLIENT_DELETE

    # This will help you to disable change functionality
    def has_change_permission(self, request, obj=None):
        return config.CLIENT_UPDATE
        
    search_fields = [
        'name',
        'payment_reference'
    ]
    list_display = (
        '__str__',
        'active',
        'payment_reference',
    )

    class Meta:
        model = Client
        

admin.site.register(Client, ClientAdmin)

