from django.contrib.sites.shortcuts import get_current_site

from constance import config
from rest_framework import serializers
from rest_framework.reverse import reverse as api_reverse

from clients.models import Client


class ClientSerializer(serializers.ModelSerializer):

    client_uri = serializers.SerializerMethodField()

    class Meta:
        model = Client
        fields = ('id', 'active', 'name', 'payment_reference', 'wallet', 'creator', 'client_uri')
        read_only_fields = ('id', 'payment_reference', 'fcm_key', 'wallet', 'creator', 'client_uri')
    
    def get_client_uri(self, obj):
        return self.context['request'].build_absolute_uri("/api/enterprise/clients/{id}/".format(id=obj.id))
        # request = self.context.get('request')
        # return api_reverse('enterprise-clients-detail', kwargs={'id': obj.id}, request=request) e.g. -list, -detail, python manage.py show_urls

    def validate_name(self, value):

        if len(value) == 0:
            raise serializers.ValidationError("Name is required")
        else:
            try:
                Client.objects.get(name=value)
                raise serializers.ValidationError("Name is already in use")
            except:
                pass

        return value
                
    def create(self, validated_data):
            
        user = self.context['request'].user
        if user.is_authenticated:

            # with transaction.atomic():

            validated_data['creator'] = user
            
            try:
                instance = Client.objects.create(**validated_data)
            except Exception as e:
                raise serializers.ValidationError(e) 
    
            return instance
        
        raise serializers.ValidationError('User needs to be signed in')

    def update(self, instance, validated_data):

        user = self.context['request'].user
        if user.is_authenticated:

            if user.is_superuser or user.is_staff or user == instance.creator:
                for attr, value in validated_data.items():
                    setattr(instance, attr, value)
                    
                instance.save()
            
                return instance

            raise serializers.ValidationError('User is not permitted')

        raise serializers.ValidationError('User needs to be signed in')
