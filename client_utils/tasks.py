from django.template.loader import render_to_string
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site
from django.core.exceptions import ValidationError
from django.core.mail import EmailMultiAlternatives
from django.db import transaction
from django.utils import timezone

from celery import shared_task
from celery.exceptions import SoftTimeLimitExceeded

from client_event_logs.models import ClientEvent
    
from constance import config


def client_email_notification(
        status: str = '',
        invoice: bool = False,
        subject: str = '', 
        message_greeting: str='', 
        message_body: str='', 
        invoice_reference: str='',
        message_footer: str='', 
        view_in_browser_url: str='', 
        email_from: str='', 
        email_from_name: str='', 
        email_to: str='', 
        reply_to: str='', 
        reply_to_name: str='',
        customer_name: str='',
        client_name: str='',
        payment_reference: str='',
        invoice_date: str='',
        packages: list=[],
        amount = 0.00,
        tax_or_vat_price = 0.00
    ):
    '''
    message
    '''

    try:

        variables = {
            'logo': config.SITE_LOGO,
            'cover_image': config.SITE_COVER_IMAGE,
            'message_greeting': message_greeting.strip(),
            'message_body': message_body,
            'customer_name': customer_name,
            'client_name': client_name,
            'payment_reference': payment_reference,
            'invoice_reference': invoice_reference,
            'invoice_date': invoice_date,
            'view_in_browser_url': view_in_browser_url.strip(),
            'status': status,
            'packages': packages,
            'currency_symbol': config.CLIENT_CURRENCY_SYMBOL,
            'amount': amount,
            'tax_or_vat_price': tax_or_vat_price,
            'total_price': float(amount) + float(tax_or_vat_price),
            'message_footer': message_footer.strip(),
            'facebook': config.SITE_FACEBOOK_LINK,
            'twitter': config.SITE_TWITTER_LINK,
            'instagram': config.SITE_INSTAGRAM_LINK,
            'linkedin': config.SITE_LINKEDIN_LINK,
            'site_name': config.SITE_NAME,
            'site_ts_and_cs': config.SITE_TS_AND_CS_LINK,
            'site_faq': config.SITE_FAQ_LINK,
            'site_unsubscribe': config.SITE_UNSUBSCRIBE_LINK,
        }

        if email_to == '':
            return ValidationError('Recipient email is needed')


        if email_from == '':
            return ValidationError('Email from is needed')
            
        if email_from_name != '':
            email_from = email_from_name.strip() + ' ' + ' <' + email_from + '>'
        

        if reply_to == '':
            reply_to = email_from
        
        if reply_to_name != '':
            reply_to = reply_to_name.strip() + ' ' + ' <' + reply_to + '>'

        message = EmailMultiAlternatives(
            subject=subject.strip(), 
            body=message_body, 
            from_email=email_from, 
            to=[email_to], 
            headers={'Reply-To': reply_to}
        )
        if invoice:
            message.attach_alternative(render_to_string('invoice.html', variables), 'text/html')
        else:
            message.attach_alternative(render_to_string('default.html', variables), 'text/html')

        message.send()

        # msg = EmailMultiAlternatives(
        #     subject="Djrill Message",
        #     body="This is the text email body",
        #     from_email="Sender <djrill@example.com>",
        #     # to=["Recipient One <someone@example.com>", "another.person@example.com"],
        #     to=["Recipient One <recipient@example.com>", ],
        #     headers={'Reply-To': "Service <support@example.com>"} # optional extra headers
        # )
        # msg.attach_alternative("<p>This is the HTML email body</p>", "text/html")
        #
        # # Optional Mandrill-specific extensions:
        # msg.tags = ["one tag", "two tag", "red tag", "blue tag"]
        # msg.metadata = {'user_id': "8675309"}

    except Exception as e:
        print(e)
        ClientEvent.objects.create(
            action = 'client_email_notification',
            details = str(e),
            data = {"email_to":email_to, "invoice":invoice, "status":status, "subject":subject, "message_body":message_body, "view_in_browser_url":view_in_browser_url}
        )

@shared_task(name="client_contract_priviledged_user_notification", time_limit=7200, soft_time_limit=7200, max_retries=5)
def client_contract_priviledged_user_notification(id=0, protocol="https", domain=None, subject='', greeting='', sign_off='', email_from='', email_to='', url="", client_name="", invited_by=""):

    from client_priviledged_contract_users.models import ClientPriviledgedContractUsers

    try:
        
        object = ClientPriviledgedContractUsers.objects.get(id=id)

        variables = {
            'protocol': protocol,
            'domain': domain,
            'pk': id,
            'url_link': url,
            'logo': config.SITE_LOGO,
            'cover_image': config.SITE_COVER_IMAGE,
            'message_greeting': greeting.strip(),
            'message_body': "You have been invited to handle the following contract - {} by {}".format(client_name),
            'message_footer': sign_off.strip(),
            'facebook': config.SITE_FACEBOOK_LINK,
            'twitter': config.SITE_TWITTER_LINK,
            'instagram': config.SITE_INSTAGRAM_LINK,
            'linkedin': config.SITE_LINKEDIN_LINK,
            'site_name': config.SITE_NAME,
            'site_ts_and_cs': config.SITE_TS_AND_CS_LINK,
            'site_faq': config.SITE_FAQ_LINK,
            'site_unsubscribe': config.SITE_UNSUBSCRIBE_LINK,
        }

        message = EmailMultiAlternatives(
            subject=subject.strip(), 
            body="message_body", 
            from_email='No Reply <' + email_from + '>', 
            to=[email_to]
        )
        
        try:
            message.attach_alternative(render_to_string('invite.html', variables), 'text/html')
            email_response = message.send()
            object.email_response = '\n\n{} - {}'.format(timezone.localtime(timezone.now()), str(email_response))

        except Exception as e:

            object.email_response = '\n\n{} - Exception: {}'.format(timezone.localtime(timezone.now()), str(e))
            ClientEvent.objects.create(
                action = 'client_contract_priviledged_user_notification',
                details = str(e),
                data = {"date":str( timezone.localtime(timezone.now()) ), "email_to":email_to, 'object':'ClientPriviledgedContractUsers - attach_alternative', 'id': id, 'invited_by': invited_by, 'client_name': client_name, 'protocol': protocol, 'domain': domain, 'url_link': url}
            )
        
        object.save()

    except SoftTimeLimitExceeded:

        client_contract_priviledged_user_notification.retry(countdown=30)
        
    except Exception as e:

        print(str(e))
        ClientEvent.objects.create(
            action = 'client_contract_priviledged_user_notification',
            details = str(e),
            data = {"date":str( timezone.localtime(timezone.now()) ), "email_to":email_to, 'object':'ClientPriviledgedContractUsers', 'id': id, 'invited_by': invited_by, 'client_name': client_name, 'protocol': protocol, 'domain': domain, 'url_link': url}
        )


@shared_task(name="send_client_emails", time_limit=7200, soft_time_limit=7200, max_retries=5)
def send_client_emails(user_id, site_id,  client_id=0, packages=[], amount=0, tax_or_vat_price=0, origin='send_client_emails', recipients=[], invoice=False, invoice_reference='', invoice_date = '', details='', status=0, subject='', message_body='', view_in_browser_url=''):

    from client_invoices.models import ClientInvoice
    from client_messages.models import ClientMessage
    from clients.models import Client
    
    message_greeting = config.CLIENT_EMAIL_GREETING
    
    try:
        status = ClientInvoice._STATUS_TYPE[int(status)]
        
        for recipient in recipients:

            try:
                recipient = get_user_model().objects.get(id=recipient)

                if config.CLIENT_EMAIL_GREETING_INCLUDE_NAME:
                    message_greeting = message_greeting + " " + recipient.first_name + ", "

                client = Client.objects.get(id=client_id)

                ClientMessage.objects.create(
                    client=client,
                    recipient=recipient, 
                    subject=subject, 
                    message = message_body, 
                    origin=origin, 
                    logs=status + " " + details
                )

                client_email_notification(
                    status = status,
                    invoice = invoice,
                    subject = subject, 
                    message_greeting = message_greeting, 
                    message_body = message_body, 
                    invoice_reference = invoice_reference,
                    message_footer = config.CLIENT_EMAIL_FOOTER, 
                    view_in_browser_url = view_in_browser_url , 
                    email_from = config.CLIENT_EMAIL_FROM, 
                    email_from_name = config.CLIENT_EMAIL_FROM_NAME,
                    email_to = recipient.email,
                    reply_to = config.CLIENT_EMAIL_REPLY_TO, 
                    reply_to_name = config.CLIENT_EMAIL_REPLY_TO_NAME, 
                    customer_name = '{} {}'.format(client.creator.first_name, client.creator.last_name),
                    client_name = client.name,
                    payment_reference = client.payment_reference,
                    invoice_date = invoice_date,
                    packages = packages,
                    amount = amount,
                    tax_or_vat_price = tax_or_vat_price
                )
            
            except Exception as e:
                print(e)
                ClientEvent.objects.create(
                    action = 'send_client_emails',
                    details = str(e),
                    data = {"origin":origin, "details":details, "status":status, "subject":subject, "message_body":message_body, "view_in_browser_url":view_in_browser_url, "recipients": recipients, 'site_id': site_id, 'user_id':user_id, 'recipient': recipient.id}
                )
    
    except Exception as e:
        
        try:
            ClientEvent.objects.create(
                action = 'send_client_emails',
                details = str(e),
                data = {"origin":origin, 'site_id': site_id, 'user_id':user_id, "details":details, "status":status, "subject":subject, "message_body":message_body, "view_in_browser_url":view_in_browser_url, "recipients": recipients}
            )
        except:
            print(e)


@shared_task(name="send_client_contract_expiry_reminder", time_limit=7200, soft_time_limit=7200, max_retries=5)
def send_client_contract_expiry_reminder():

    from client_contracts.models import ClientContract
    from client_invoices.models import ClientInvoice
    from client_priviledged_contract_users.models import ClientPriviledgedContractUsers

    from datetime import date

    contracts = ClientContract.objects.filter(active=True)

    try:

        for contract in contracts:

            if contract.start:

                with transaction.atomic():

                    amount = contract.package.get_price()

                    days_before = (contract.end() - timezone.now()).days

                    #10 days before contract ends - Customer invoice
                    if days_before <= 10:

                        invoice = ClientInvoice.get_or_create_invoice(contract, amount, 'Customer invoice')
                        
                        details = ''
                        subject = ''
                        status = 0
                        wallet_transaction = None
                        
                        #10 days before - invoice payment reminder
                        if days_before == 10:
                            # generate invoice and send
                            details='This is a notice that an invoice has been generated on {} due on {}'.format( date.today().strftime("%B %d, %Y"), contract.end().strftime("%d/%m/%Y %H:%M:%S") )
                            subject = 'Customer Invoice - #{}'.format(contract.contract_reference)

                        #3 days before - invoice payment reminder
                        elif days_before == 3:
                            # check if wallet is set before sending
                            if contract.client.wallet >= amount:
                                details = 'This is a billing reminder that your invoice no. {} which was generated on {} is due on {}. Please load money into your wallet.'.format( invoice.invoice_reference, invoice.created.strftime("%d/%m/%Y %H:%M:%S"), contract.end().strftime("%d/%m/%Y %H:%M:%S") )
                                subject = 'Invoice Payment Reminder - #{}'.format(contract.contract_reference)

                        elif days_before < 0:

                            # check if wallet is set before deducting amount or sending termination
                            if contract.client.wallet >= amount:# and wallet is healthy deduct:
                                details = "Payment ({}{}) was paid using wallet auto reminder payment. It was taken from the client's wallet".format(config.CLIENT_CURRENCY_SYMBOL, amount)
                                subject = 'Invoice Paid - #{}'.format(invoice.invoice_reference)
                                wallet_transaction = 1
                                status=1
                                contract.start = None
                                contract.active = True

                            # 3 days before expiry ends - second overdue
                            elif contract.days_remaining() == 3:
                                details = 'This is a billing reminder that your invoice no. {} which was generated on {} is now overdue'.format( invoice.invoice_reference, invoice.created.strftime("%d/%m/%Y %H:%M:%S") )
                                subject = 'First Invoice Overdue Notice - #{}'.format(contract.contract_reference)

                            # 1 day before expiry ends - last overdue
                            elif contract.days_remaining() == 1:
                                details = 'This is the follow up billing reminder that your invoice no. {} which was generated on {} is now overdue'.format( invoice.invoice_reference, invoice.created.strftime("%d/%m/%Y %H:%M:%S") )
                                subject = 'Follow up Invoice Overdue Notice - #{}'.format(contract.contract_reference)

                            else:
                                # terminate
                                details = 'This is a notification that your service has now been suspended. The details of this suspension are below'
                                subject = 'Service Suspension Notification - #{}'.format(contract.contract_reference)
                                status=2
                                contract.active = False
   
                        if details != '':
                            users = list( ClientPriviledgedContractUsers.objects.filter(contract=contract, accepted=True, active=True, can_read_contract_invoices=True).values_list('user_id', flat=True) )
                            
                            contract.save(invoice=invoice)
                            contract.invoice_update_and_mail_notificaton(
                                recipients=users,
                                invoice=invoice,
                                details=details,
                                status=status,
                                subject=subject,
                                view_in_browser_url=config.SITE_URL + '/api/enterprise/client-invoices/{}/'.format(invoice.id),
                                amount=amount,
                                wallet_transaction=wallet_transaction
                            )
        
    except SoftTimeLimitExceeded:

        send_client_contract_expiry_reminder.retry(countdown=30)
        
    except Exception as e:

        print('send_client_contract_expiry_reminder')
        print(str(e))
        ClientEvent.objects.create(
            action = 'send_client_contract_expiry_reminder',
            details = str(e),
            data = {"date":str( timezone.localtime(timezone.now()) )}
        )