import re
import string
from random import randint, choice

from constance import config


def set_config_value(key, value):
    """
    update settings
    :param key
    :param value
    :return: None
    """
    setattr(config, key, value)


def get_config_value(key):
    """
    get settings
    :param key
    :return: value
    """
    return getattr(config, key)


def get_field_choices(named):
    """
    Get choices from namedtuple
    """
    dictionary = named._asdict()
    return [(v, k) for (k, v) in dictionary.items()]


def random_generator(length=5, letters=True, digits=False, punctuation=False, exclude_characters=[]):
    '''
    Create random string of any length
    exclude_characters - list of string characters to leave out e.g [':', '<', '8'] 
    letters - boolean value to include alphabets or not
    digits - boolean value to include alphabets or not
    punctuation - boolean value to include alphabets or not
    '''
    
    allchar = ''

    if letters:
        # allchar = string.ascii_letters 
        allchar = string.ascii_uppercase 
    if digits:
        allchar += string.digits 
    if punctuation:
        allchar += string.punctuation

    if not letters and not digits and not punctuation:
        allchar = string.ascii_uppercase

    exclude_characters.append('0')
    exclude_characters.append('O')

    generator = "".join(choice(allchar) for x in range(randint(length, length)) if str(x) not in exclude_characters)
    
    return generator


def replace_puntcuation(word='', replace_with=''):
    '''
    Replace punctuation and spaces
    '''
    # translator = str.maketrans(string.punctuation, replace_with*len(string.punctuation)) #map punctuation with character
    replacements = {string.punctuation: '', ' ':replace_with}
    return re.sub(r'[ _]', lambda m: replacements[m[0]], word)


