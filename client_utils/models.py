from django.db import models

from django.utils import timezone

# Create your models here.

# note that we use the Django-recommended abstract base class pattern
# in other words: Mixin also inherits from models.Model, but specifies
# abstract=True in Meta.
class CreatedModifiedMixin(models.Model):
    """
    Django abstract class mixin for having models automatically
    maintain a created and modified datetime stamp.

    mixin based on this Stack Overflow answer:
    http://stackoverflow.com/questions/1737017/django-auto-now-and-auto-now-add

    :author: Kabelo Twala

    """

    created = models.DateTimeField(editable=False, default=timezone.now)
    modified = models.DateTimeField(editable=False, default=timezone.now)

    def save(self, *args, **kwargs):
        if not self.id:
            self.created = timezone.now()

        self.modified = timezone.now()
        super(CreatedModifiedMixin, self).save(*args, **kwargs)

    class Meta:
        abstract = True


class PermissionsMixin(models.Model):
    """
    Django abstract class mixin for access permissions defined on models.
    """
    #:author: Kabelo Twala

    @classmethod
    def has_permission(cls, request):
        return False

    def has_object_permission(self, request):
        return False

    class Meta:
        abstract = True

'''
--SoftDelete--
client
contracts
priviledged
'''
