from client_utils.models import PermissionsMixin

from rest_framework.permissions import BasePermission, SAFE_METHODS


class IsStaffOrSuperuser(BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """

    model = None
    message = "You don't have permission to access"

    def has_permission(self, request, view):
        if request.user.is_superuser or request.user.is_staff:
            return True

        if request.user and request.method == 'GET':
            return True

        return False

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        # if request.method in SAFE_METHODS:
        #     return True

        # Write permissions are only allowed to the superuser and owner of the object.
        if request.user.is_superuser or request.user.is_staff:
            return True

        return False
            

class ModelBasedPermissions(BasePermission):
    """
    Method and object permissions based on Model.
    """
    # :author: Kabelo  Twala <kabelo@twineflair.com> for Twine Flair
    model = None

    def has_permission(self, request, view):
        if not request.user.is_authenticated:
            return False

        assert self.model is not None, \
            "'%s' must include a 'model' attribute" % self.__class__.__name__

        assert issubclass(self.model, PermissionsMixin), \
            "The 'model' attribute of '%s' must inherit from PermissionsMixin" % self.__class__.__name__

        return self.model.has_permission(request)

    def has_object_permission(self, request, view, obj):
        if not request.user.is_authenticated:
            return False

        return obj.has_object_permission(request, obj)


def get_privileges_exists_check(user):

    try:
        return user.client_priviledged_user.filter(accepted=True, active=True).exists()
    except:
        return False       
    
def get_privileges(user):

    try:
        return user.client_priviledged_user.filter(accepted=True, active=True)
    except:
        return None

def is_privileged_on_other_contract_users(obj=None, user=None, method='GET'):

    try:

        if user.is_superuser or user.is_staff:
            return True


        if method == 'GET':

            if obj:
                return user.client_priviledged_user.filter(contract_id=obj.contract.id, accepted=True, active=True, can_read_other_contract_users=True).exists()

            return user.client_priviledged_user.filter(accepted=True, active=True, can_read_other_contract_users=True).exists()

        elif method == 'POST':

            if obj:
                return user.client_priviledged_user.filter(contract_id=obj.contract.id, accepted=True, active=True, can_invite_other_contract_users=True).exists()

            return user.client_priviledged_user.filter(accepted=True, active=True, can_invite_other_contract_users=True).exists()

        else:
            # cant change client profile creator unless you are the client creator
            if obj.user == obj.contract.client.creator:
                if user == obj.contract.client.creator:
                    return True
                return False

            # cant change contract creator unless you are the contract creator or client creator
            if obj.user == obj.contract.creator:
                if user == obj.contract.creator or user == obj.contract.client.creator:
                    return True
                return False

            if method == 'PUT':
                return user.client_priviledged_user.filter(contract_id=obj.contract.id, accepted=True, active=True, can_update_other_contract_users=True).exists()

            elif method == 'DELETE':
                return user.client_priviledged_user.filter(contract_id=obj.contract.id, accepted=True, active=True, can_delete_other_contract_users=True).exists()

            elif method == 'PATCH':
                return user.client_priviledged_user.filter(contract_id=obj.contract.id, accepted=True, active=True, can_update_other_contract_users=True).exists()

            return False
    except:
        return False
        
def is_privileged_parent_link_tree(user, obj=None):
    # no particular application for this as yet
    # recursively check if priviledged_user is linked to current user making check
    try:

        if user == user.granted_by:
            return True

        parent = obj.granted_by
        while True:

            if parent == parent.granted_by:
                return False
                
            if user == parent:
                return True

            parent = parent.granted_by

    except:
        return False

