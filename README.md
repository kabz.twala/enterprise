# Enterprise

A system that offers subscription packages to clients

## Compatibility

* Django 3.2
* Python 3.6+

## Setup

Run

    sudo pip install virtualenv virtualenvwrapper
    source virtualenvwrapper.sh

Create a new virtual environment

    mkvirtualenv enterprise

Install required packages

    pip install -r requirements.txt

Create .env file in root dir of project that stores your sensitive data (Email address below is used to send out mail)

    SECRET_KEY = 'django-insecure-2@d6sk_f6@v3fy$xy!4u@2&p*ciobb6i*fj7q2s88apkinq%@t'

    PV_EMAIL_HOST = 'smtp.gmail.com' 
    PV_EMAIL_HOST_USER = 'your.email@gmail.com'
    PV_EMAIL_HOST_PASSWORD = 'your-password'
    PV_EMAIL_PORT = 587
    PV_EMAIL_USE_TLS = True

Migrate django models:

    python manage.py makemigrations
    python manage.py migrate

Create Superuser 

    python manage.py createsuperuser

Start

    python manage.py runserver

## High level rundown
Go to the rootapi e.g. [/api/enterprise/](http://localhost:8000/api/enterprise/)

> AS ADMIN OR STAFF, create:

* Sites (optional) - [/api/enterprise/client-sites/](http://localhost:8000/api/enterprise/client-sites/)

    Sites can be used to distinguish which packages should be displayed on which site. 

* Category (required) - [/api/enterprise/client-package-categories/](http://localhost:8000/api/enterprise/client-package-categories/)

    Your product offering also used to categorize packages. e.g. Payment Gateway, Ticketing System, or more
    Atleast one is required. 

* Package(s) (required) - [/api/enterprise/client-packages/](http://localhost:8000/api/enterprise/client-packages/)

    Package examples for Payment Gateway: Card payments, Crypto payments, Cardless payments. 
    Package examples for Ticketing System: Live Chat, Reporting, Support
    

> AS USER OR ADMIN, create:

* Client (required) - [/api/enterprise/clients/](http://localhost:8000/api/enterprise/clients/)

    Users create client accounts

* Contract(s) (required) - [/api/enterprise/client-contracts/](http://localhost:8000/api/enterprise/client-contracts/)

    Clients create contracts by selecting a package. 
    Customized specific for client e.g. logo, css file, fcm_key api key, etc. 
    Clients can create more than one contract

* Priviledged users (optional) - [/api/enterprise/client-priviledged-contract-users/](http://localhost:8000/api/enterprise/client-priviledged-contract-users/)

    Clients can invite new or existing users via email to help manage their contract(s)


> CURRENTLY ADMIN OR STAFF can do the following (using the terminal):

    [get admin access_token]
    curl http://localhost:8000/api/token/ -d "username=admin_username&password=password" 

    [get client payment_reference (admin dashboard) and top up account]
    curl -X POST -d "payment_reference=payment_reference&amount=50" http://localhost:8000/api/enterprise/client-wallet-transactions/top_up/ -H "Authorization: Bearer ***REPLACE WITH access_token***"
    
    [get invoice_reference (admin dashboard) after creating contract and make invoice payment]
    curl -X POST -d "invoice_reference=invoice_reference&status=1&details=Paying" http://localhost:8000/api/enterprise/client-invoices/make_invoice_update/ -H "Authorization: Bearer ***REPLACE WITH access_token***"
    

## Roadmap
Ideas for releases in the future, 

* Add basic bootstrap GUI
* Create installable package

