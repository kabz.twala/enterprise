from rest_framework import serializers

from client_messages.models import ClientMessage


class ClientMessageSerializer(serializers.ModelSerializer):

    class Meta:
        model = ClientMessage
        fields = ('id', 'client', 'recipient', 'subject', 'message', 'seen', 'origin', 'logs')

    def create(self, validate_data):
        
        return ClientMessage.objects.create(**validate_data)
    
    def update(self, instance, validated_data):
        return 