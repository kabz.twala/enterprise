from django.conf import settings
from django.db import models
from django.contrib.sites.shortcuts import get_current_site

from enterprise.middleware.get_request import get_request
from client_utils.models import CreatedModifiedMixin
from clients.models import Client

 
class ClientMessage(CreatedModifiedMixin):

    client = models.ForeignKey(Client, null=True, blank=True, on_delete=models.SET_NULL)
    
    recipient = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    subject = models.CharField(max_length=128, null=True, blank=True)
    message = models.TextField(null=True, blank=True)

    seen = models.BooleanField(default=False)

    origin = models.TextField(blank=True, null=True)

    logs = models.TextField(null=True, blank=True)

    def __str__(self):
        return '{}. {}'.format(self.id, self.subject)

    @classmethod
    def get_queryset(cls, user):
        request = get_request()

        if not user.is_authenticated:
            return ClientMessage.objects.none()

        if user.is_superuser or user.is_staff:
            return ClientMessage.objects.all()

        #return ClientMessage.objects.filter(recipient=user.id, client__site=get_current_site(request))
        return ClientMessage.objects.filter(recipient=user.id)

    @classmethod
    def has_permission(cls, request):
        """ 
        Permissions for users.
        """

        if request.user.is_superuser or request.user.is_staff:
            return True


    def has_object_permission(self, request, obj):
        """ 
        Object Permissions for users.
        """

        if request.user.is_superuser or request.user.is_staff:
            return True
    