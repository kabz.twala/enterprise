from django.utils import timezone

from rest_framework import decorators, status, viewsets
from rest_framework.response import Response
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated

from client_messages.models import ClientMessage
from client_messages.serializers import ClientMessageSerializer

from client_utils.permissions import ModelBasedPermissions


class ClientMessagePermissions(ModelBasedPermissions):
    model = ClientMessage
    message = "You don't have permission to access messages"


class ClientMessageViewSet(viewsets.ModelViewSet):

    model = ClientMessage
    serializer_class = ClientMessageSerializer
    permission_classes = (IsAuthenticated, )
    #pagination_class = ClientCustomPagination
    search_fields = (
        'client__name',
        'subject',
        'message',
    )
    ordering_fields = (
        'id',
        'seen',
        'created',
        'modified'
    )

    def get_queryset(self):
        
        if not self.request.user.is_authenticated:
            raise PermissionDenied()

        queryset = ClientMessage.get_queryset(self.request.user)

        if queryset is not None:
            return queryset.order_by('-modified')

        raise PermissionDenied()
        
    @decorators.action(detail=False, methods=['patch'], permission_classes = (IsAuthenticated, ))
    def viewed(self, request):
        
        try:
            api_message_id = request.data.get('api_message_id')

            message = ClientMessage.objects.get(id=api_message_id, recipient=request.user)

            message.seen = True
            message.logs = "{}\n\n{} - message viewed".format(message.logs, timezone.localtime(timezone.now()))

            message.save()

            return Response({'detail': 'Updated'}, status=status.HTTP_200_OK)

        except Exception as e:
            return Response({'detail': 'Not permitted - {}'.format(str(e))}, status=status.HTTP_401_UNAUTHORIZED)