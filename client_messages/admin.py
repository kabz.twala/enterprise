from django.contrib import admin

from client_messages.models import ClientMessage

class MessageAdmin(admin.ModelAdmin):
    
    def has_add_permission(self, request, obj=None):
        return False
    
    # This will help you to disable delete functionaliyt
    # def has_delete_permission(self, request, obj=None):
    #     return False

    # This will help you to disable change functionality
    def has_change_permission(self, request, obj=None):
        return False
        
    class Meta:
        model = ClientMessage
        
    search_fields = [
        'subject',
        'message',
        'recipient',
        'origin',
        'logs',
    ]
    list_display = (
        '__str__',
        'message',
        'recipient',
        'seen',
    )

admin.site.register(ClientMessage, MessageAdmin)
